//
//  PopupDevelopingViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 25/06/2022.
//

struct PopupDevelopingViewBuilder {
    static func build() -> PopupDevelopingViewController {
        let rootVC = PopupDevelopingViewController(nibName: "PopupDevelopingViewController", bundle: nil)
        return rootVC
    }
}

