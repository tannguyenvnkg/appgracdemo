//
//  WalletViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

import UIKit

class WalletViewController: BaseViewController {

    private let popupDeveloping = PopupDevelopingViewBuilder.build()
    override func viewDidLoad() {
        super.viewDidLoad()

        showNavigationBar(showBackButon: true,showQuestionButton: true,title: "VÍ")
        
        configView()
    }

    private func configView(){
        popupDeveloping.callbackDismissed = { [weak self] in
            self?.tabBarController?.selectedIndex = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        popupDeveloping.show()
    }

}
