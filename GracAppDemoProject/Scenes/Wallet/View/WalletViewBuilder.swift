//
//  WalletBuilder.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

struct WalletViewBuilder {
    static func build() -> WalletViewController {
        let rootVC = WalletViewController(nibName: "WalletViewController", bundle: nil)
        return rootVC
    }
}
