//
//  UserInfoViewController.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 28/06/2022.
//

import UIKit

class UserInfoViewController: BaseViewController {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var customerIDLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var customerAddressLabel: UILabel!
    @IBOutlet weak var contractIDLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var collectorLabel: UILabel!
    @IBOutlet weak var collectorPhoneLabel: UILabel!
    @IBOutlet weak var billingCircleLabel: UILabel!
    @IBOutlet weak var transportationFeeLabel: UILabel!
    @IBOutlet weak var collectionFeeLabel: UILabel!
    @IBOutlet weak var destructionFeeLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var momoFeeLabel: UILabel!
    @IBOutlet weak var totalFeeLabel: UILabel!
    @IBOutlet weak var paymentsLabel: UILabel!
    
    @IBOutlet weak var garbageSortingPlace: UIImageView!
    @IBOutlet weak var garbageSortingPlaceHeight: NSLayoutConstraint!
    @IBOutlet weak var momoPaymentButton: UIButton!
    @IBOutlet weak var vietelPaymentButton: UIButton!
    @IBOutlet weak var invoiceButton: UIButton!
    @IBOutlet weak var updateRequestButton: UIButton!
    
//    private let garbageSortingPlaceHeight = 150
    private var viewModel: SearchUserViewModel!
    private var province: AddressModel!
    private var option: OptionSearch!
    private var content: String!
    private var userInfo: SearchUserModel!
    
    func inject(viewModel: SearchUserViewModel) {
        self.viewModel = viewModel
    }
    
    func bindData(userInfo: SearchUserModel){
        self.userInfo = userInfo
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setDatatoView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideTabbar()
        showNavigationBar(showQuestionButton: true,title: "THÔNG TIN KHÁCH HÀNG")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    private func setupUI(){
        containerView.layer.cornerRadius = 5
        momoPaymentButton.layer.cornerRadius = 5
        vietelPaymentButton.layer.cornerRadius = 5
        invoiceButton.layer.cornerRadius = 5
        updateRequestButton.layer.cornerRadius = 5
        
        
        //add underline
        customerIDLabel.addUnderline(spacing: 2)
        customerNameLabel.addUnderline(spacing: 2)
        customerAddressLabel.addUnderline(spacing: 2)
        contractIDLabel.addUnderline(spacing: 2)
        serviceLabel.addUnderline(spacing: 2)
        collectorLabel.addUnderline(spacing: 2)
        collectorPhoneLabel.addUnderline(spacing: 2)
        billingCircleLabel.addUnderline(spacing: 2)
        transportationFeeLabel.addUnderline(spacing: 2)
        collectionFeeLabel.addUnderline(spacing: 2)
        destructionFeeLabel.addUnderline(spacing: 2)
        taxLabel.addUnderline(spacing: 2)
        momoFeeLabel.addUnderline(spacing: 2)
        totalFeeLabel.addUnderline(spacing: 2)
        paymentsLabel.addUnderline(spacing: 2)
        
        if userInfo.isRegistration == 0 {
            garbageSortingPlaceHeight.constant = 0
        }
        
        if userInfo.isPaid == 1 {
            updateRequestButton.isHidden = true
        }
        
    }
    
    private func setDatatoView(){
        self.customerIDLabel.text = userInfo.userCode
        self.customerNameLabel.text = userInfo.userName
        self.customerAddressLabel.text = userInfo.address
        self.contractIDLabel.text = userInfo.contractCode
        self.serviceLabel.text = userInfo.serviceCode
        self.collectorLabel.text = userInfo.staffName
        self.collectorPhoneLabel.text = userInfo.staffPhone
        self.billingCircleLabel.text = userInfo.billingCircle
        self.transportationFeeLabel.text = userInfo.transportFee
        self.collectionFeeLabel.text = userInfo.collectionFee
        self.destructionFeeLabel.text = userInfo.destructionFee
        self.taxLabel.text = "\(userInfo.tax)%"
        self.momoFeeLabel.text = "\(userInfo.momoFee)"
        self.totalFeeLabel.text = userInfo.totalMoney
    }
    
    @IBAction func updateRequestButtonPressed(_ sender: UIButton) {
        let vc = UpdateRequestViewBuilder.build()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func momoPaymentButtonPressed(_ sender: UIButton) {
        let url = URL(string: "https://grac.vn/huong-dan-thanh-toan-tien-rac-qua-vi-momo/")
        let vc = GracWebViewBuilder.build(url: url!)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func vietelPaymentButtonPressed(_ sender: UIButton) {
        let url = URL(string: "https://grac.vn/huong-dan-thanh-toan-tien-rac-qua-viettel-money/")
        let vc = GracWebViewBuilder.build(url: url!)
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func invoiceButtonPressed(_ sender: UIButton) {
        let userCode = userInfo.userCode.filter { $0 != "-" }
        let url = URL(string: "https://grac.vn/in-giay-bao-tien-rac/?code=\(userCode)")
        let vc = GracWebViewBuilder.build(url: url!)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
