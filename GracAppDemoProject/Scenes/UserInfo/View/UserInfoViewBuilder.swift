//
//  UserInfoViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 28/06/2022.
//

import Foundation

struct UserInfoViewBuilder {
    static func build(userInfo: SearchUserModel) -> UserInfoViewController {
        let rootVC = UserInfoViewController(nibName: "UserInfoViewController", bundle: nil)
        rootVC.bindData(userInfo: userInfo)
        return rootVC
    }
}
