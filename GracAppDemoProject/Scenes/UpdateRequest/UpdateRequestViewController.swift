//
//  UpdateRequestViewController.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 05/07/2022.
//

import UIKit

class UpdateRequestViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var userCodeTextField: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var bookingButton: UIButton!
    @IBOutlet weak var complaintButton: UIButton!
    private let placeHolderTextColor = UIColor(rgb: 0x6B6B6B)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideTabbar()
        showNavigationBar(showQuestionButton: true,title: "YÊU CẦU CẬP NHẬP")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    private func setupUI(){
        containerView.layer.cornerRadius = 5
        submitButton.layer.cornerRadius = 5
        bookingButton.layer.cornerRadius = 5
        complaintButton.layer.cornerRadius = 5
        
        userCodeTextField.textColor = UIColor.darkText
        userCodeTextField.font = .systemFont(ofSize: 14)
        userCodeTextField.attributedPlaceholder = NSAttributedString(
            string: "Mã khách hàng",
            attributes: [NSAttributedString.Key.foregroundColor: placeHolderTextColor]
        )
        
        categoryTextField.textColor = UIColor.darkText
        categoryTextField.font = .systemFont(ofSize: 14)
        categoryTextField.attributedPlaceholder = NSAttributedString(
            string: "Đăng ký phân loại rác",
            attributes: [NSAttributedString.Key.foregroundColor: placeHolderTextColor]
        )
        
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func bookingButtonPressed(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func complaintButtonPressed(_ sender: UIButton) {
        let vc = ComplaintViewBuilder.build()
        navigationController?.pushViewController(vc, animated: true)
    }
    

}
