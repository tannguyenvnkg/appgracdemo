//
//  UpdateRequestViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 05/07/2022.
//

import Foundation

struct UpdateRequestViewBuilder {
    static func build() -> UpdateRequestViewController {
        let rootVC = UpdateRequestViewController(nibName: "UpdateRequestViewController", bundle: nil)
        return rootVC
    }
}
