//
//  GracWebViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/8/22.
//

import UIKit
import WebKit
class GracWebViewController: BaseViewController {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var closeButton: UIImageView!
    
    private var url: URL?
    private var isLandscape: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideTabbar()
        hideNavigationBar()
        
        configView()
        
        webView.scrollView.delegate = self
        webView.navigationDelegate = self
        showLoadingView()
        
        guard let url = self.url else{return}
        webView.load(URLRequest(url: url))
    }
    
    override var shouldAutorotate: Bool{
        return self.isLandscape
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // landscape for phone
        if isLandscape {
            let landscapeLeft = UIInterfaceOrientation.landscapeLeft.rawValue
            UIDevice.current.setValue(landscapeLeft, forKey: "orientation")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // back to portrait
        if isLandscape {
            let portrait = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(portrait, forKey: "orientation")
        }
    }

    func bindData(url: URL, isLandscape: Bool = false){
        self.url = url
        self.isLandscape = isLandscape
    }

    private func configView(){
        closeButton.isHidden = !isLandscape
        closeButton.formatActionView(selector: #selector(closeVC), target: self)
    }
    
    @objc private func closeVC(){
        print("close")
        _ = navigationController?.popViewController(animated: true)
    }
}

extension GracWebViewController : WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideLoadingView()
        showNavigationBar(title: "GRAC")
    }
}

extension GracWebViewController : UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0
        {
            // scroll to top
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
        else
        {
            // scroll to bottom
            navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
}
