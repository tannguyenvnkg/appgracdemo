//
//  GracViewBuilder.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/8/22.
//

import Foundation

struct GracWebViewBuilder {
    static func build(url: URL, isLandscape: Bool = false) -> GracWebViewController{
        let rootVC = GracWebViewController(nibName: "GracWebViewController", bundle: nil)
        rootVC.bindData(url: url,isLandscape: isLandscape)
        return rootVC
    }
}
