//
//  ContactViewBuilder.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/15/22.
//

struct ContactViewBuilder {
    static func build() -> ContactViewController{
        let rootVC = ContactViewController(nibName: "ContactViewController", bundle: nil)
        return rootVC
    }
}
