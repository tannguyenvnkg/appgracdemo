//
//  ContactViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/15/22.
//

import UIKit
import StoreKit
class ContactViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    
    private let arrayTitle = ["1900 0340", "Zalo", "Gmail", "Link về web grac.vn"]
    private let arrayImage = ["ic-phone-contact", "ic-zalo", "ic-gmail", "ic-link"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.layer.cornerRadius = 10
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ContactTableViewCell.getNib(), forCellReuseIdentifier: ContactTableViewCell.identifier)
        tableView.rowHeight = 65
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showTabbar()
        showNavigationBar(showQuestionButton: true,title: "LIÊN HỆ")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    private func makeCall(){
        let phoneNumber = "19000340"
        guard let url = URL(string: "telprompt:\(phoneNumber)") else { print("Url error"); return }
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else {
            print("không thế open tel")
        }
    }
    
    private func openZalo(){
        let zaloPhone = "0908090013"
        guard let url = URL(string: "https://zalo.me/\(zaloPhone)") else { print("Url error"); return }
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
              UIApplication.shared.openURL(url)
            }
        }
        else { // link to Zalo on Appstore
            let zaloId = 579523206
            let vc = SKStoreProductViewController()
            vc.loadProduct(withParameters: [SKStoreProductParameterITunesItemIdentifier: NSNumber(value:zaloId) ],completionBlock: nil)
            present(vc, animated: true)
        }
    }
    
    private func sendMail(){
        let email = "congnghe@grac.vn"
        guard let url = URL(string: "mailto:\(email)") else { print("Url error"); return }
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
              UIApplication.shared.open(url)
            } else {
              UIApplication.shared.openURL(url)
            }
        }else {
            print("không thế open send mail")
        }
    }
    
    private func goToGracWeb(){
        let url = URL(string: "https://grac.vn/")
        let gracWebVC = GracWebViewBuilder.build(url: url!)
        navigationController?.pushViewController(gracWebVC, animated: true)
    }
}

//MARK: UITableViewDelegate,UITableViewDataSource
extension ContactViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactTableViewCell.identifier, for: indexPath) as! ContactTableViewCell
    
        cell.setData(title: arrayTitle[indexPath.row],image: arrayImage[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            makeCall()
        case 1:
            openZalo()
        case 2:
            sendMail()
        case 3:
            goToGracWeb()
        default:
            goToGracWeb()
        }
 
    }
    
}
