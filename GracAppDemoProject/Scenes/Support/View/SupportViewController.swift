//
//  SupportViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

import UIKit

class SupportViewController: BaseViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var complaintButton: UIButton!
    @IBOutlet weak var collectionRegistrationButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
   
        setupUI()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavigationBar(showBackButon: true,showQuestionButton: true,title: "HỖ TRỢ")
        showTabbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    
    func setupUI(){
        // rounded Container View
        containerView.layer.cornerRadius = 5
        // rounded Button
        contactButton.layer.cornerRadius = 20
        
        complaintButton.backgroundColor = .clear
        complaintButton.layer.cornerRadius = 20
        complaintButton.layer.borderWidth = 1
        complaintButton.layer.borderColor = UIColor(rgb: 0x46914A).cgColor
        
        collectionRegistrationButton.backgroundColor = .clear
        collectionRegistrationButton.layer.cornerRadius = 20
        collectionRegistrationButton.layer.borderWidth = 1
        collectionRegistrationButton.layer.borderColor = UIColor(rgb: 0x46914A).cgColor
    }
    
    @IBAction func contactButtonPressed(_ sender: UIButton) {
        let contactVC = ContactViewBuilder.build()
        navigationController?.pushViewController(contactVC, animated: true)
    }
    
    @IBAction func complaintButtonPressed(_ sender: UIButton) {
        let complaintVC = ComplaintViewBuilder.build()
        navigationController?.pushViewController(complaintVC, animated: true)
    }
    
    @IBAction func collectionRegistrationButtonPressed(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 2
    }
}
