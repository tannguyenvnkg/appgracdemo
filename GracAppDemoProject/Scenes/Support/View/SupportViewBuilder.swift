//
//  SupportBuilder.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

struct SupportViewBuilder {
    static func build() -> SupportViewController {
        let rootVC = SupportViewController(nibName: "SupportViewController", bundle: nil)
        return rootVC
    }
}
