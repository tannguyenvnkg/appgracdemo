//
//  DumpGarbageWayTableViewCell.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/16/22.
//

import UIKit

class DumpGarbageWayTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImageView: UIImageView!
    
    static let identifier = "DumpGarbageWayTableViewCell"
    
    static func getNib() -> UINib{
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(title: String, image: String){
        titleLabel.text = title
        titleImageView.image = UIImage(named: image)
    }
}
