//
//  DumpGarbageWayViewBuilder.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/16/22.
//

struct DumpGarbageWayViewBuilder {
    static func build() -> DumpGarbageWayViewController {
        let rootVC = DumpGarbageWayViewController(nibName: "DumpGarbageWayViewController", bundle: nil)
        return rootVC
    }
}
