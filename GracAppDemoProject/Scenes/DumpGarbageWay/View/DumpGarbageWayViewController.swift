//
//  DumpGarbageWayViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/16/22.
//

import UIKit

class DumpGarbageWayViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private let arrayTitle = ["Phân loại rác tại nguồn","Rác sinh hoạt thông thường", "Rác thu gom đặc biệt",
                              "Rác tái chế", "Điểm thu hồi"]
    
    private let arrayImage = ["ic-cachborac-1","ic-cachborac-2","ic-cachborac-3","ic-cachborac-1","ic-cachborac-5"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        containerView.layer.cornerRadius = 10
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(DumpGarbageWayTableViewCell.getNib(), forCellReuseIdentifier: DumpGarbageWayTableViewCell.identifier)
        tableView.rowHeight = 65
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showTabbar()
        showNavigationBar(showQuestionButton: true,title: "CÁCH ĐỔ RÁC")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }

}


extension DumpGarbageWayViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DumpGarbageWayTableViewCell.identifier, for: indexPath) as! DumpGarbageWayTableViewCell
    
        cell.setData(title: arrayTitle[indexPath.row], image: arrayImage[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var urlString = ""
        
        switch indexPath.row {
        case 0:
            urlString = "https://grac.vn/danh-muc/phan-loai-rac-tai-nguon/"
        case 1:
            urlString = "https://grac.vn/danh-muc/rac-sinh-hoat-thong-thuong/"
        case 2:
            urlString = "https://grac.vn/danh-muc/rac-thu-gom-dac-biet/"
        case 3:
            urlString = "https://grac.vn/danh-muc/rac-tai-che/"
        case 4:
            urlString = "https://grac.vn/danh-muc/diem-thu-hoi/"
        default:
            urlString = "https://grac.vn/"
        }
        
        let url = URL(string: urlString)
        let gracWebVC = GracWebViewBuilder.build(url: url!)
        navigationController?.pushViewController(gracWebVC, animated: true)
    }
    
}
