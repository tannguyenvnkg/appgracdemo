//
//  PopupMessageViewController.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 24/06/2022.
//

import UIKit

class PopupGarbageMessageViewController: BasePopupViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var titlePopupLabel: UILabel!
    @IBOutlet weak var closePopupButton: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        configView()
    }

    private func configView(){
        closePopupButton.formatActionView(selector: #selector(closePopup), target: self)
        backgroundView.formatActionView(selector: #selector(closePopup), target: self)
        
        let attributedString = NSMutableAttributedString(string: contentLabel.text!)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 15
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        contentLabel.attributedText = attributedString
    }
    
    private func setupUI(){
        containerView.layer.cornerRadius = 5
    }
    
    @objc func closePopup(){
        self.hide()
    }
}
