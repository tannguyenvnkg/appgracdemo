//
//  PopupMessageViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 24/06/2022.
//

struct PopupGarbageMessageViewBuilder {
    static func build() -> PopupGarbageMessageViewController {
        let rootVC = PopupGarbageMessageViewController(nibName: "PopupGarbageMessageViewController", bundle: nil)
        return rootVC
    }
}
