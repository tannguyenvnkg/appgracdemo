//
//  News.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 21/06/2022.
//

class NewsModel{
    var image: String = ""
    var day: String = ""
    var month: String = ""
    var type: String = ""
    var title: String = ""
    var content: String = ""
    var link: String = ""
    static func build(image: String, day: String, month: String, type: String, title: String, content: String, link: String) -> NewsModel{
        let news = NewsModel()
        news.image = image
        news.day = day
        news.month = month
        news.type = type
        news.title = title
        news.content = content
        news.link = link
        return news
    }
    
    static func getData() -> [NewsModel]{
        let arrayNews = [
            NewsModel.build(image: "van-ban-phap-ly", day: "18", month: "4", type: "GIÁ TIỀN RÁC, TIN TỨC", title: "Căn cứ pháp lý tiền rác TP.HCM", content: "Giá dịch vụ thu gom, vận chuyển, xử lý chất thải rắn sinh hoạt  (viết tắt là tiền rác) đối với hộ gia đình, chủ nguồn thải dựa vào các căn cứ pháp lý từ trung ương đến địa phương. Giá dịch vụ vận chuyển và xử lý chất thải rắn được các địa phương không trợ giá, trợ giá một phần hoặc toàn phần.", link: "https://grac.vn/can-cu-phap-ly-tien-rac-tp-hcm/"),
            NewsModel.build(image: "green-money", day: "07", month: "3", type: "DỊCH VỤ, GIÁ TIỀN RÁC, TIN TỨC", title: "Quản lý giá thu gom rác và các loại phí bảo vệ môi trường", content: "Phần mềm  GRAC quản lý giá thu gom rác căn cứ theo quy định nào ? Theo Luật Bảo vệ môi trường 2020 có hiệu lực từ ngày 01/01/2022, việc thu phí xử lý rác sinh hoạt được thực hiện chậm nhất trước ngày 31/12/2024. Theo đó, giá dịch vụ thu gom, vận chuyển và xử lý chất thải rắn sinh hoạt từ hộ gia đình, cá nhân được tính toán theo căn cứ sau đây:", link: "https://grac.vn/quan-ly-gia-thu-gom-rac-va-cac-loai-phi-bao-ve-moi-truong/"),
            NewsModel.build(image: "gia-rac", day: "01", month: "12", type: "GIÁ TIỀN RÁC, QUẢN LÝ RÁC THẢI, TIN TỨC", title: "Phương pháp tính giá rác TP.HCM theo Quyết định 20/2021/QĐ-UBND TP HCM", content: "Hộ gia đình, chủ nguồn thải có thể tham khảo cách tính khối lượng rác theo khối lượng hoặc dung tích thùng rác quy đổi",link: "https://grac.vn/phuong-phap-tinh-theo-quyet-dinh-20-2021-qd-ubnd-tp-hcm/")
        ]
        return arrayNews
    }
}
