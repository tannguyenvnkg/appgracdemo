//
//  NewsViewController.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 21/06/2022.
//

import UIKit

class NewsViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let arrayNews = NewsModel.getData()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        containerView.layer.cornerRadius = 10
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(NewsCollectionViewCell.getNib(), forCellWithReuseIdentifier: NewsCollectionViewCell.identifier)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showTabbar()
        showNavigationBar(showQuestionButton: true,title: "TIN TỨC")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
}

extension NewsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayNews.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NewsCollectionViewCell.identifier, for: indexPath) as! NewsCollectionViewCell
        let news = arrayNews[indexPath.row]

        cell.setData(image: news.image, day: news.day, month: news.month, type: news.type, title: news.title, content: news.content, link: news.link)
        
        cell.callBackGoToGracWeb = { [weak self] link in
            let url = URL(string: link)
            let gracWebVC = GracWebViewBuilder.build(url: url!)
            self?.navigationController?.pushViewController(gracWebVC, animated: true)
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/2 - 5
        return CGSize(width: width, height: 370)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let link = arrayNews[indexPath.row].link
        let url = URL(string: link)
        let gracWebVC = GracWebViewBuilder.build(url: url!)
        self.navigationController?.pushViewController(gracWebVC, animated: true)
    }
}
