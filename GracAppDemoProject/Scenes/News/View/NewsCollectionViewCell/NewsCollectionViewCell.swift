//
//  NewsCollectionViewCell.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 21/06/2022.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var link : String = ""
    
    var callBackGoToGracWeb: ((String) -> ())?
    
    static let identifier = "NewsCollectionViewCell"
    static func getNib() -> UINib{
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 10
        
        image.clipsToBounds = true
        image.layer.cornerRadius = 10
        image.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    func setData(image: String, day: String, month: String, type: String, title: String, content: String, link: String = ""){
        self.image.image = UIImage(named: image)
        dayLabel.text = day
        monthLabel.text = "TH\(month)"
        typeLabel.text = type
        titleLabel.text = title
        contentLabel.text = content
        self.link = link
    }

    @IBAction func keepReadingButtonPressed(_ sender: UIButton) {
        self.callBackGoToGracWeb?(link) 
    }

}
