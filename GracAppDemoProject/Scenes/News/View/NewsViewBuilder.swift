//
//  NewsViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 21/06/2022.
//

struct NewsViewBuilder {
    static func build() -> NewsViewController {
        let rootVC = NewsViewController(nibName: "NewsViewController", bundle: nil)
        return rootVC
    }
}
