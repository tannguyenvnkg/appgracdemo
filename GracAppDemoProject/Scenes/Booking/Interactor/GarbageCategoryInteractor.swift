//
//  GarbageInteractor.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 30/06/2022.
//
import Foundation
import RxSwift

protocol GarbageCategoryInteractor {
    func getGarbageCategory() -> Single<([GarbageCategoryModel])>
    func getSpecialGarbageCategory() -> Single<([GarbageCategoryModel])>
    func addNewGarbageCollectionSchedule(userBooking: UserBookingModel, pictures: [UIImage]) -> Single<BookingModel>
}
