//
//  BookingModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 02/07/2022.
//

struct BookingModel: Decodable {
    var accept: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case accept
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        container.decodeIfPresent(Bool.self, forKey: .accept, assignTo: &accept)
    }
    
}
