//
//  UserBookingModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 02/07/2022.
//

import Foundation

struct UserBookingModel{
    var customerName: String
    var customerPhone: String
    var customerEmail: String
    var garbageCategoryID: String
    var description: String
    var specialGarbageCategoryID: String
    var provinceID: String
    var districtID: String
    var wardID: String
    var shortAddress: String
    var capacityOfGarbage: String
    var collectionDate: String

    init(customerName: String, customerPhone: String, customerEmail: String, garbageCategoryID: String, description: String, specialGarbageCategoryID: String, provinceID: String, districtID: String, wardID: String, shortAddress: String, capacityOfGarbage: String, collectionDate: String) {
        self.customerName = customerName
        self.customerPhone = customerPhone
        self.customerEmail = customerEmail
        self.garbageCategoryID = garbageCategoryID
        self.description = description
        self.specialGarbageCategoryID = specialGarbageCategoryID
        self.provinceID = provinceID
        self.districtID = districtID
        self.wardID = wardID
        self.shortAddress = shortAddress
        self.capacityOfGarbage = capacityOfGarbage
        self.collectionDate = collectionDate
    }
}
