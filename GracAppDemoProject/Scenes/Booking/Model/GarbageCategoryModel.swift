//
//  GarbageModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 24/06/2022.
//

struct GarbageCategoryModel: Codable {
    var id: Int = -1
    var title: String = ""
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
    }
    
    init(id: Int, title: String){
        self.id = id
        self.title = title
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        container.decodeIfPresent(Int.self, forKey: .id, assignTo: &id)
        container.decodeIfPresent(String.self, forKey: .title, assignTo: &title)
    }
    
    
    static func getGarbage() -> [GarbageCategoryModel]{
        let arrayGarbage = [
            GarbageCategoryModel(id: 1, title: "Ký hợp đồng thu rác mới"),
            GarbageCategoryModel(id: 2, title: "Rác cồng kềnh"),
            GarbageCategoryModel(id: 3, title: "Rác xây dựng"),
            GarbageCategoryModel(id: 4, title: "Rác phát sinh thêm"),
            GarbageCategoryModel(id: 5, title: "Rác ve chai, đồng nát")
        ]
        return arrayGarbage
    }
    
    static func getSpecialGarbage() -> [GarbageCategoryModel]{
        let arrayGarbage = [
            GarbageCategoryModel(id: 1, title: "Pin thải"),
            GarbageCategoryModel(id: 2, title: "Thiết bị điện tử"),
            GarbageCategoryModel(id: 3, title: "Vỏ hộp sữa"),
            GarbageCategoryModel(id: 4, title: "Nhớt thải"),
            GarbageCategoryModel(id: 5, title: "Dầu ăn thải")
        ]
        return arrayGarbage
    }
}
