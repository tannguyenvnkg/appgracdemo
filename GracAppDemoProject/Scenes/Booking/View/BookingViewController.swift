//
//  BookingViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

import UIKit

class BookingViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var specialGarbageView: UIView!
    @IBOutlet weak var garbageView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    
    private var garbageVC: BookingForGarbageViewController!
    private var specialGarbageVC: BookingForSpecialGarbageViewController!
    private var viewModel: GarbageCategoryViewModel!
    
    func inject(viewModel: GarbageCategoryViewModel) {
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configView()
        setupBinding()
        setupUI()
        getData() // call api
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showTabbar()
        showNavigationBar(showBackButon: true,showQuestionButton: true,title: "CHỌN DANH MỤC RÁC")
        containerView.layer.cornerRadius = 5
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }

    //MARK: - config and binding
    private func configView(){
        garbageVC = BookingForGarbageViewBuilder.build()
        specialGarbageVC = BookingForSpecialGarbageViewBuilder.build()
        
        addChildViewController(controller: garbageVC!, containerView: self.garbageView)
        addChildViewController(controller: specialGarbageVC!, containerView: self.specialGarbageView)
        
        garbageVC?.cellSelected = { [weak self] in
            guard let indexPath = self?.specialGarbageVC?.tableView.indexPathForSelectedRow else { return }
            self?.specialGarbageVC?.tableView.deselectRow(at: indexPath, animated: false)
        }
        
        specialGarbageVC?.cellSelected = { [weak self] in
            guard let indexPath = self?.garbageVC?.tableView.indexPathForSelectedRow else { return }
            self?.garbageVC?.tableView.deselectRow(at: indexPath, animated: false)
        }
    }
    
    private func setupBinding() {
        viewModel.emitEventError
            .subscribeNext { [weak self] error in
                guard self != nil else {return}
                if error.code == 6 {
                    self?.errorConnectionBanner.show()
                }
                Logger.log("Error: \(error.message)")
            }.disposed(by: rx.disposeBag)
        
        viewModel.emitEventLoading
            .subscribeNext { [weak self] isLoading in
                guard let self = self else {return}
                Logger.log("Loading: \(isLoading)")
                if isLoading {
                    self.showLoadingView()
                } else {
                    self.hideLoadingView()
                }
            }.disposed(by: rx.disposeBag)
        
        viewModel.getGarbageDataTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                self.garbageVC.updateList(listCategory: data)
                self.garbageVC.reloadList()
            }.disposed(by: rx.disposeBag)
        
        viewModel.getSpecialGarbageDataTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                self.specialGarbageVC.updateList(listCategory: data)
                self.specialGarbageVC.reloadList()
            }.disposed(by: rx.disposeBag)
    }
    
    private func setupUI(){
        if #available(iOS 15.0, *) {
            nextButton.configuration?.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
                            var outgoing = incoming
                            outgoing.font = UIFont.systemFont(ofSize: 16, weight: .bold)
                            return outgoing
                        }
        }else{
            nextButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        }
        
        nextButton.layer.cornerRadius = 5
        
    }
    private func getData(){
        viewModel.getGarbageData()
        viewModel.getSpecialGarbageData()
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if let indexPath = self.garbageVC?.tableView.indexPathForSelectedRow {
            let garbage = self.garbageVC!.listCategory[indexPath.row]
            let detailGarbageVC = DetailBookingViewBuilder.build(garbage: garbage, garbageCategory: .normal)
            navigationController?.pushViewController(detailGarbageVC, animated: true)
        }else if let indexPath = self.specialGarbageVC?.tableView.indexPathForSelectedRow {
            let garbage = self.specialGarbageVC!.listCategory[indexPath.row]
            let detailGarbageVC = DetailBookingViewBuilder.build(garbage: garbage, garbageCategory: .special)
            navigationController?.pushViewController(detailGarbageVC, animated: true)
        }
    }
}
