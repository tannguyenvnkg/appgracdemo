//
//  GarbageViewController.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 24/06/2022.
//

import UIKit

class BookingForGarbageViewController: UIViewController {

    @IBOutlet weak var questionButton: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var cellSelected: (()->())?
//    var listCategory: [GarbageCategoryModel] = GarbageCategoryModel.getGarbage()
    var listCategory: [GarbageCategoryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(BookingGarbageTableViewCell.getNib(), forCellReuseIdentifier: BookingGarbageTableViewCell.identifier)
        
        questionButton.formatActionView(selector: #selector(showQuestionPopup), target: self)
    }
    
    func updateList(listCategory: [GarbageCategoryModel]){
        self.listCategory.append(contentsOf: listCategory)
    }
    
    func reloadList(){
        self.tableView.reloadData()
    }
    
    @objc func showQuestionPopup(){
        let popup = PopupGarbageMessageViewBuilder.build()
        popup.show()
    }
}

extension BookingForGarbageViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookingGarbageTableViewCell.identifier, for: indexPath) as! BookingGarbageTableViewCell
    
        cell.setData(title: listCategory[indexPath.row].title)
                
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.cellSelected?()
    }
}
