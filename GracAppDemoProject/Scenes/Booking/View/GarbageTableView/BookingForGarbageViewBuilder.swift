//
//  BookingForGarbageViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 24/06/2022.
//
struct BookingForGarbageViewBuilder {
    static func build() -> BookingForGarbageViewController {
        let bookingForGarbageVC = BookingForGarbageViewController(nibName: "BookingForGarbageViewController", bundle: nil)
        return bookingForGarbageVC
    }
}
