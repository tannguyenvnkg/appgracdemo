//
//  BookingForSpecialGarbageViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 24/06/2022.
//

struct BookingForSpecialGarbageViewBuilder {
    static func build() -> BookingForSpecialGarbageViewController {
        let bookingForSpecialGarbageVC = BookingForSpecialGarbageViewController(nibName: "BookingForSpecialGarbageViewController", bundle: nil)
        return bookingForSpecialGarbageVC
    }
}
