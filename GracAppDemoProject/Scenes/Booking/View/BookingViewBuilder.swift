//
//  BookingBuilder.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

struct BookingViewBuilder {
    static func build() -> BookingViewController {
        let rootVC = BookingViewController(nibName: "BookingViewController", bundle: nil)
        let interactor = GarbageCategoryInteractorImpl()
        let viewModel = GarbageCategoryViewModel(interactor: interactor)
        rootVC.inject(viewModel: viewModel)
        return rootVC
    }
}
