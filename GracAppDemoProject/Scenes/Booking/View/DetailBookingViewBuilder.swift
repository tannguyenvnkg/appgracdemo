//
//  DetailBookingViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 24/06/2022.
//
enum GarbageCategory {
    case normal
    case special
}
struct DetailBookingViewBuilder {
    static func build(garbage: GarbageCategoryModel, garbageCategory: GarbageCategory = .normal) -> DetailBookingViewController {
        let rootVC = DetailBookingViewController(nibName: "DetailBookingViewController", bundle: nil)
        let interactor = GarbageCategoryInteractorImpl()
        let viewModel = GarbageCategoryViewModel(interactor: interactor)
        rootVC.inject(viewModel: viewModel)
        rootVC.bindData(garbage: garbage, garbageCategory: garbageCategory)
        return rootVC
    }
}
