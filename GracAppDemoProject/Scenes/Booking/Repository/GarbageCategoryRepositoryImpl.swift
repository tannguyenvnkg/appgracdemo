//
//  GarbageRepositoryImpl.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 30/06/2022.
//

import Foundation
import RxSwift
import Moya

class GarbageCategoryRepositoryImpl: GarbageCategoryRepository {
    let moyaPlugins = [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))]
    lazy var apiProvider = MoyaProvider<GarbageAPI>(plugins: moyaPlugins)
    
    func getGarbageCategory() -> Single<([GarbageCategoryModel])> {
        apiProvider.request(.getGarbageCategory, response: ResponseWrapper<[GarbageCategoryModel]>.self)
    }
    
    func getSpecialGarbageCategory() -> Single<([GarbageCategoryModel])> {
        apiProvider.request(.getSpecialGarbageCategory, response: ResponseWrapper<[GarbageCategoryModel]>.self)
    }
    
    func addNewGarbageCollectionSchedule(userBooking: UserBookingModel, pictures: [UIImage]) -> Single<BookingModel> {
        apiProvider.request(.addNewGarbageCollectionSchedule(userBooking: userBooking, pictures: pictures), response: ResponseWrapper<BookingModel>.self)
    }
}
