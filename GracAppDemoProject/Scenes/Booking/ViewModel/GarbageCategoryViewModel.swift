//
//  GarbageViewModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 30/06/2022.
//


import RxSwift
import RxCocoa
import Action
import NSObject_Rx

class GarbageCategoryViewModel: NSObject {
    
    private let interactor: GarbageCategoryInteractor
    let emitEventError = PublishRelay<(APIError)>()
    let emitEventLoading = PublishRelay<(Bool)>()
    
    private lazy var getGarbageDataAction = makeupGarbageDataAction()
    let getGarbageDataTrigger = PublishRelay<([GarbageCategoryModel])>()
    
    private lazy var getSpecialGarbageDataAction = makeupSpecialGarbageDataAction()
    let getSpecialGarbageDataTrigger = PublishRelay<([GarbageCategoryModel])>()
    
    private lazy var getAddingNewBookingDataAction = makeAddingNewBookingDataAction()
    let getAddingNewBookingDataTrigger = PublishRelay<(BookingModel)>()
    
    init(interactor: GarbageCategoryInteractorImpl) {
        self.interactor = interactor
        super.init()
        configGetGarbageData()
        configGetSpecialGarbageData()
        configAddingNewBookingData()
    }
    
    //MARK: API to update data
    func getGarbageData() {
        getGarbageDataAction.execute()
    }
    
    func getSpecialGarbageData() {
        getSpecialGarbageDataAction.execute()
    }
    
    func bookGarbageCollection(userBooking: UserBookingModel, pictures: [UIImage]){
        getAddingNewBookingDataAction.execute((userBooking,pictures))
    }
    
    private func makeupGarbageDataAction() -> Action<(), [GarbageCategoryModel]> {
        return Action<(), [GarbageCategoryModel]> { [weak self] data in
            guard let self = self else { return Observable.empty() }
            return self.interactor.getGarbageCategory().asObservable()
        }
    }
    
    private func makeupSpecialGarbageDataAction() -> Action<(), [GarbageCategoryModel]> {
        return Action<(), [GarbageCategoryModel]> { [weak self] data in
            guard let self = self else { return Observable.empty() }
            return self.interactor.getSpecialGarbageCategory().asObservable()
        }
    }
    
    private func makeAddingNewBookingDataAction() -> Action<(UserBookingModel,[UIImage]), BookingModel> {
        return Action<(UserBookingModel,[UIImage]), BookingModel> { [weak self] data in
            guard let self = self else { return Observable.empty() }
            return self.interactor.addNewGarbageCollectionSchedule(userBooking: data.0, pictures: data.1).asObservable()
        }
    }

    private func configGetGarbageData() {
        getGarbageDataAction.elements
            .bind(to: getGarbageDataTrigger)
            .disposed(by: rx.disposeBag)
        
        getGarbageDataAction
            .errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                self?.emitEventError.accept(error)
        }
        .disposed(by: rx.disposeBag)
        
        getGarbageDataAction
            .executing
            .subscribeNext { [weak self] executing in
                self?.emitEventLoading.accept(executing)
        }
        .disposed(by: rx.disposeBag)
    }
    
    private func configGetSpecialGarbageData() {
        getSpecialGarbageDataAction.elements
            .bind(to: getSpecialGarbageDataTrigger)
            .disposed(by: rx.disposeBag)
        
        getSpecialGarbageDataAction
            .errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                self?.emitEventError.accept(error)
        }
        .disposed(by: rx.disposeBag)
        
        getSpecialGarbageDataAction
            .executing
            .subscribeNext { [weak self] executing in
                self?.emitEventLoading.accept(executing)
        }
        .disposed(by: rx.disposeBag)
    }
    
    private func configAddingNewBookingData() {
        getAddingNewBookingDataAction.elements
            .bind(to: getAddingNewBookingDataTrigger)
            .disposed(by: rx.disposeBag)
        
        getAddingNewBookingDataAction
            .errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                self?.emitEventError.accept(error)
        }
        .disposed(by: rx.disposeBag)
        
        getAddingNewBookingDataAction
            .executing
            .subscribeNext { [weak self] executing in
                self?.emitEventLoading.accept(executing)
        }
        .disposed(by: rx.disposeBag)
    }
}
