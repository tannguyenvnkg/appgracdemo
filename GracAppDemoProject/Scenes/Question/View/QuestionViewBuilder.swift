//
//  QuestionBuilder.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

struct QuestionViewBuilder {
    static func build() -> QuestionViewController {
        let rootVC = QuestionViewController(nibName: "QuestionViewController", bundle: nil)
        return rootVC
    }
}
