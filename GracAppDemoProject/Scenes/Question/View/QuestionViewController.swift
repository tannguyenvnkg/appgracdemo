//
//  QuestionViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

import UIKit

class QuestionViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private let arrayTitle = ["Tiền rác hiện nay ở TP Hồ Chí Minh", "Tôi có thể đóng tiền rác",
                              "Văn bản đóng tiền rác"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.layer.cornerRadius = 10
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(QuestionTableViewCell.getNib(), forCellReuseIdentifier: QuestionTableViewCell.identifier)
        tableView.rowHeight = 65
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showTabbar()
        showNavigationBar(showBackButon: true,showQuestionButton: true,title: "CÂU HỎI THƯỜNG GẶP")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }

}

extension QuestionViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: QuestionTableViewCell.identifier, for: indexPath) as! QuestionTableViewCell
    
        cell.setData(title: arrayTitle[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var urlString = ""
        
        switch indexPath.row {
        case 0,1,2:
            urlString = "https://grac.vn/cau-hoi-thuong-gap/"
        
        default:
            urlString = "https://grac.vn/"
        }
        
        let url = URL(string: urlString)
        let gracWebVC = GracWebViewBuilder.build(url: url!)
        navigationController?.pushViewController(gracWebVC, animated: true)
        
    }
    
}
