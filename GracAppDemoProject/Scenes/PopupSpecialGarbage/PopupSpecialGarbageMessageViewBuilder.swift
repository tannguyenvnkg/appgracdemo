//
//  PopupSpecialGarbageMessageViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 25/06/2022.
//

struct PopupSpecialGarbageMessageViewBuilder {
    static func build() -> PopupSpecialGarbageMessageViewController {
        let rootVC = PopupSpecialGarbageMessageViewController(nibName: "PopupSpecialGarbageMessageViewController", bundle: nil)
        return rootVC
    }
}

