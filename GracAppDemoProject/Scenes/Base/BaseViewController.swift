//
//  BaseViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/10/22.
//

import UIKit
import NotificationBannerSwift
class BaseViewController: UIViewController {
    private var loadingVC: LoadingViewController?
    
    var errorConnectionBanner: NotificationBanner!
    var fillFieldErrorBanner: NotificationBanner!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // set color for navigation bar
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            // Light Mode
            overrideUserInterfaceStyle = .light
        }
        
        setNavigationBarColor()
        configTabbar()
        configStatusBar()
        configNotificationBanner()
    }
    private func configTabbar(){
        if #available(iOS 15.0, *) {
            let appearance = UITabBarAppearance()
            appearance.configureWithOpaqueBackground()
            self.tabBarController?.tabBar.standardAppearance = appearance
            self.tabBarController?.tabBar.scrollEdgeAppearance = tabBarController?.tabBar.standardAppearance
        }
        
        self.tabBarController?.tabBar.barTintColor = UIColor.white
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    
    private func configStatusBar(){
        
        //background color
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let statusBar = UIView(frame: window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        statusBar.backgroundColor = UIColor(rgb: 0x1B9340)
        window?.addSubview(statusBar)
      
    }
    
    func configNotificationBanner(){
        errorConnectionBanner = NotificationBanner(title: "LỖI KẾT NỐI", subtitle: "Không thể kết nối Internet.", style: .danger)
        errorConnectionBanner.duration = 3
        
        fillFieldErrorBanner = NotificationBanner(title: "THÔNG BÁO", subtitle: "Vui lòng điền đầy đủ thông tin", style: .danger)
        fillFieldErrorBanner.duration = 2
    }
    private func setNavigationBarColor(){
        if #available(iOS 15.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor(rgb: 0x1B9340)
            appearance.shadowColor =  .clear
            appearance.titleTextAttributes = [ .foregroundColor: UIColor.white ]
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
            navigationController?.navigationBar.tintColor = .white
            
        }else{
            navigationController?.navigationBar.barTintColor = UIColor(rgb: 0x1B9340)
            navigationController?.navigationBar.titleTextAttributes = [
                .foregroundColor: UIColor.white,
            ]
            navigationController?.navigationBar.tintColor = .white
        }
    }
    
    func hideNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func showNavigationBar(showBackButon: Bool? = false, showQuestionButton: Bool? = false, title: String? = ""){
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = title
        if showBackButon! {
            let image = scaleImage(imageName: "ic-back")
            let backButtonItem = UIBarButtonItem(image: image,
                                                 style: .plain,
                                                 target: self,
                                                 action: #selector(backButtonHomeTouch))
            
            navigationItem.leftBarButtonItem = backButtonItem
        }
        
        if showQuestionButton! {
            let image = scaleImage(imageName: "ic-question-navigation")
            let rightButtonItem = UIBarButtonItem(image: image,
                                                  style: .plain,
                                                  target: self,
                                                  action: #selector(showDevelopingPopup))
            
            navigationItem.rightBarButtonItem = rightButtonItem
        }
    }
    
    private func scaleImage(imageName: String) -> UIImage{
        let image  = UIImage(named: imageName)
        let size = CGSize(width: 20, height: 20)
        let imageScale = image?.scalePreservingAspectRatio(targetSize: size)
        return imageScale!
    }
    
    func hideTabbar(){
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func showTabbar(){
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @objc func backButtonHomeTouch() {
        self.tabBarController?.selectedIndex = 0
    }
    
    @objc private func showDevelopingPopup() {
        let popup = PopupDevelopingViewBuilder.build()
        popup.show()
    }
    
    func showLoadingView() {
        if loadingVC == nil {
            loadingVC = LoadingViewController(nibName: "LoadingViewController", bundle: nil)
        }
        loadingVC?.show()
    }
    
    func hideLoadingView() {
        loadingVC?.hide()
    }
    
    @objc private func hideLoading() {
        loadingVC?.dismiss(animated: false, completion: nil)
    }
}
