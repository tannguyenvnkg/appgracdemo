//
//  BasePopup.swift
//
//  Created by Hung Hai Hoang Mai Vu on 1/27/21.
//

import UIKit

class BasePopupViewController: UIViewController {
    
    var viewController = UIViewController()
    private var isShowing = false
    var isHideWhenTapBackground = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            overrideUserInterfaceStyle = .light
        }
        if isHideWhenTapBackground {
            let tap = UITapGestureRecognizer(target: self, action: #selector(hide as () -> Void))
            tap.numberOfTouchesRequired = 1
            tap.cancelsTouchesInView = true
            self.view.addGestureRecognizer(tap)
        }
    }
    
    func show() {
        self.show(nil)
    }
    
    func show(_ completion: (() -> Void)?) {
        if !isShowing {
            isShowing = true
            let currentWindow = UIApplication.shared.windows.first { $0.isKeyWindow }
            viewController.view.backgroundColor = .clear
            view.backgroundColor = .clear
            viewController.view.addSubview(self.view)
            viewController.addChild(self)
            didMove(toParent: viewController)
            view.frame = viewController.view.frame
            currentWindow?.addSubview(self.viewController.view)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [.transitionCrossDissolve, .allowAnimatedContent], animations: { [weak self] in
                self?.viewController.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            })
            
            self.view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.view.alpha = 0.0;
              
            UIView.animate(withDuration: 0.3) {
                    self.view.alpha = 1.0
                    self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                } completion: { (finished) in
                    completion?()
                }
        }
        
    }
    
    @objc func hide(_ completion: (() -> Void)?) {
        if isShowing {
            isShowing = false
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.transitionCrossDissolve, .allowAnimatedContent], animations: { [weak self] in
                self?.viewController.view.backgroundColor = .clear
            })
           
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                    self.view.alpha = 0.0;
                }, completion:{ (finished : Bool) in
                    if (finished)
                    {
                        self.viewController.view.removeFromSuperview()
                        self.view.removeFromSuperview()
                        self.view.transform = CGAffineTransform.identity
                    }
                    completion?()
                })
        }
    }
    
    @objc func hide() {
        self.hide(nil)
    }
    
    func showMessageError(errorMessage: String,
                          retryCallback: (() -> Void)? = nil,
                          cancelCallback: (() -> Void)? = nil) {
        
        let message = errorMessage.isEmpty ? "Có lỗi xảy ra, vui lòng xin thử lại." : errorMessage
        
        
        let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Thử lại", style: UIAlertAction.Style.default, handler: { _ in
            DispatchQueue.main.async {
                retryCallback?()
            }
        }))
        if cancelCallback != nil {
            alert.addAction(UIAlertAction(title: "Hủy", style: UIAlertAction.Style.cancel, handler: { _ in
                DispatchQueue.main.async {
                    cancelCallback?()
                }
            }))
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.present(alert, animated: true, completion: nil)
        })
        
    }
    
    func showMessage(message: String,
                     title: String = "",
                     cancelCallback: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Đóng", style: UIAlertAction.Style.default, handler: { _ in
            DispatchQueue.main.async {
                cancelCallback?()
            }
        }))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.present(alert, animated: true, completion: nil)
        })
        
    }
    
}
