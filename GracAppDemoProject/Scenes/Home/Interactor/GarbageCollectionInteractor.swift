//
//  GarbageCollectionInteractor.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import Foundation
import RxSwift

protocol GarbageCollectionInteractor {
    func getListGarbageCollectionASC(currentPage: Int, perPage: Int) -> Single<[GarbageCollectionModel]>
    func getListGarbageCollectionDESC(currentPage: Int, perPage: Int) -> Single<[GarbageCollectionModel]>
}
