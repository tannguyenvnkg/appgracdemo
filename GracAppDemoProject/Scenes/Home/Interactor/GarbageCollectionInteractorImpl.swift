//
//  GarbageCollectionInteractorImpl.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import Foundation
import RxSwift

struct GarbageCollectionInteractorImpl: GarbageCollectionInteractor {
    let repository = GarbageCollectionRepositoryImpl()
    func getListGarbageCollectionASC(currentPage: Int, perPage: Int) -> Single<[GarbageCollectionModel]> {
        return repository.getListGarbageCollectionASC(currentPage: currentPage, perPage: perPage)
    }
    
    func getListGarbageCollectionDESC(currentPage: Int, perPage: Int) -> Single<[GarbageCollectionModel]> {
        return repository.getListGarbageCollectionDESC(currentPage: currentPage, perPage: perPage)
    }
                                                       
}
