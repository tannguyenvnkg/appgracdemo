//
//  SampleRepositoryImpl.swift
//  Hack Kanji
//
//  Created by Vu Mai Hoang Hai Hung on 4/8/20.
//  Copyright © 2020 Vu Mai Hoang Hai Hung. All rights reserved.
//

import Foundation
import RxSwift
import Moya

class GarbageCollectionRepositoryImpl: GarbageCollectionRepository {
    let moyaPlugins = [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))]
    lazy var apiProvider = MoyaProvider<GarbageAPI>(plugins: moyaPlugins)
    
    func getListGarbageCollectionASC(currentPage: Int, perPage: Int) -> Single<[GarbageCollectionModel]> {
        apiProvider.request(.getListGarbageASC(currentPage: currentPage, perPage: perPage), response: ResponseWrapper<[GarbageCollectionModel]>.self)
    }
    
    func getListGarbageCollectionDESC(currentPage: Int, perPage: Int) -> Single<[GarbageCollectionModel]> {
        apiProvider.request(.getListGarbageDESC(currentPage: currentPage, perPage: perPage), response: ResponseWrapper<[GarbageCollectionModel]>.self)
    }
    
}
