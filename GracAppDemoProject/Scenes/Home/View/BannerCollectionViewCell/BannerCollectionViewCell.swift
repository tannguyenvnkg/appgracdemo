//
//  BannerCollectionViewCell.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/9/22.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var bookGarbageCollectionButton: UIButton!
    @IBOutlet weak var searchGarbageCollectionMoneyButton: UIButton!
    
    static let identifier = "BannerCollectionViewCell"
    
    var bookingCallback: (()->())?
    var searchFeeCallback: (()->())?
    static func getNib() -> UINib{
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    func setupUI() {
        // round bottom backgroumd image
//        backgroundImageView.clipsToBounds = true
//        backgroundImageView.layer.cornerRadius = 30
//        backgroundImageView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        // round border button
        bookGarbageCollectionButton.layer.cornerRadius = 5
        searchGarbageCollectionMoneyButton.layer.cornerRadius = 5
    }

    @IBAction func bookGarbageCollectionButtonPressed(_ sender: UIButton) {
        bookingCallback?()
    }
    
    @IBAction func searchGarbageCollectionButtonPressed(_ sender: UIButton) {
        searchFeeCallback?()
    }
}
