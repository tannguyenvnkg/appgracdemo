//
//  WelcomeBuilder.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/8/22.
//

struct HomeViewBuilder {
    static func build() -> HomeViewController{
        let rootVC = HomeViewController(nibName: "HomeViewController", bundle: nil)
        return rootVC
    }
}
