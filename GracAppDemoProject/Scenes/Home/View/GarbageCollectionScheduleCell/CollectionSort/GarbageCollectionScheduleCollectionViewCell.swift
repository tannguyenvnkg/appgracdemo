//
//  GarbageCollectionScheduleCollectionViewCell.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

import UIKit
import Imaginary
class GarbageCollectionScheduleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var garbageNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var garbageImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    static let identifier = "GarbageCollectionScheduleCollectionViewCell"
    
    static func getNib() -> UINib{
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 5
        
        garbageNameLabel.text = ""
        addressLabel.text = ""
        dateLabel.text = ""
        garbageImage.image = UIImage(named: "image-error")
    }
    
    func setData(name: String, address: String, date: String, image: String) {
        // name
        if name != "" {
            garbageNameLabel.text = name
        } else {
            garbageNameLabel.text = ""
        }
        // address
        if address != "" {
            addressLabel.text = address
        }else{
            addressLabel.text = ""
        }
        
        if date != "" {
            dateLabel.text = date
        }else{
            dateLabel.text = ""
        }
        
        if image != "" {
            let placeholder = UIImage(named: Constant.placeHoderImage)
            guard let imageUrl = URL(string: image) else { return }
            garbageImage.setImage(url: imageUrl, placeholder: placeholder)
        }else{
            garbageImage.image = UIImage(named: "image-error")
        }
        
    }
    
    
}
