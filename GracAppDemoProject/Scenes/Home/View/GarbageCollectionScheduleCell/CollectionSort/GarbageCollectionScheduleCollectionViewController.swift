//
//  GarbageCollectionScheduleCollectionViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

import UIKit

class GarbageCollectionScheduleCollectionViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var listGarbageASC: [GarbageCollectionModel] = []
    private var listGarbageDESC: [GarbageCollectionModel] = []
    
    var isAscending = false
    fileprivate var activityIndicator: LoadMoreActivityIndicator!
    
    var loadMoreCallBack : (()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(GarbageCollectionScheduleCollectionViewCell.getNib(), forCellWithReuseIdentifier: GarbageCollectionScheduleCollectionViewCell.identifier)
        
        activityIndicator = LoadMoreActivityIndicator(scrollView: collectionView, spacingFromLastCell: 10, spacingFromLastCellWhenLoadMoreActionStart: 60)
        
    }
    
    func stopLoadMoreIndicator(){
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stop()
        }
    }
    
    func emptyList(){
        listGarbageASC.removeAll()
        listGarbageDESC.removeAll()
    }
    
    func updateListASC(listGarbage: [GarbageCollectionModel]){
        self.listGarbageASC.append(contentsOf: listGarbage)
    }
    
    func updateListDESC(listGarbage: [GarbageCollectionModel]){
        self.listGarbageDESC.append(contentsOf: listGarbage)
    }
    
    func reloadList(){
        self.collectionView.reloadData()
    }
}

//MARK:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension GarbageCollectionScheduleCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        return 10
        return isAscending ? listGarbageASC.count :  listGarbageDESC.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GarbageCollectionScheduleCollectionViewCell.identifier, for: indexPath) as! GarbageCollectionScheduleCollectionViewCell
        
        let item = isAscending ? listGarbageASC[indexPath.row] :  listGarbageDESC[indexPath.row]
        cell.setData(name: item.name, address: item.address, date: item.dateSubmit, image: item.picture1)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.bounds.width/2 - 1
        return CGSize(width: width, height: 350)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("item \(indexPath) selected")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        activityIndicator.start {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.loadMoreCallBack?()
            }
        }
    }
}
