//
//  GarbageCollectionFirstCellTableViewCell.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/13/22.
//

import UIKit

class GarbageCollectionScheduleTableViewCell: UITableViewCell {
    @IBOutlet weak var garbageNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var garbageImage: UIImageView!
    
    static let identifier = "GarbageCollectionScheduleTableViewCell"
    
    static func getNib() -> UINib{
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
