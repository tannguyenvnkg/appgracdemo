//
//  GarbageCollectionScheduleViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/13/22.
//

import UIKit

class GarbageCollectionScheduleTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var listGarbageASC: [GarbageCollectionModel] = []
    private var listGarbageDESC: [GarbageCollectionModel] = []
    
    var isAscending = false
    fileprivate var activityIndicator: LoadMoreActivityIndicator!
    var loadMoreCallBack : (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(GarbageCollectionScheduleTableViewCell.getNib(), forCellReuseIdentifier: GarbageCollectionScheduleTableViewCell.identifier)
        
        activityIndicator = LoadMoreActivityIndicator(scrollView: tableView, spacingFromLastCell: 10, spacingFromLastCellWhenLoadMoreActionStart: 60)
    }
    
    func stopLoadMoreIndicator(){
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stop()
        }
    }
    func emptyList(){
        listGarbageASC.removeAll()
        listGarbageDESC.removeAll()
    }
    func updateListASC(listGarbage: [GarbageCollectionModel]){
        self.listGarbageASC.append(contentsOf: listGarbage)
    }
    
    func updateListDESC(listGarbage: [GarbageCollectionModel]){
        self.listGarbageDESC.append(contentsOf: listGarbage)
    }
    
    func reloadList(){
        self.tableView.reloadData()
    }
}

extension GarbageCollectionScheduleTableViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return 10
        return isAscending ? listGarbageASC.count :  listGarbageDESC.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GarbageCollectionScheduleTableViewCell.identifier, for: indexPath) as! GarbageCollectionScheduleTableViewCell
        
        
        let item = isAscending ? listGarbageASC[indexPath.row] :  listGarbageDESC[indexPath.row]
        cell.setData(name: item.name, address: item.address, date: item.dateSubmit, image: item.picture1)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("row: \(indexPath.row)")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        activityIndicator.start {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.loadMoreCallBack?()
            }
        }
    }
}

