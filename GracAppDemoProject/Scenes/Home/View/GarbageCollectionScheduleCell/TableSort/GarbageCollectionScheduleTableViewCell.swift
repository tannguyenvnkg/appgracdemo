//
//  GarbageCollectionFirstCellTableViewCell.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/13/22.
//

import UIKit
import Imaginary
class GarbageCollectionScheduleTableViewCell: UITableViewCell {
    @IBOutlet weak var garbageNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var garbageImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    static let identifier = "GarbageCollectionScheduleTableViewCell"
    
    static func getNib() -> UINib{
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 5
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(name: String, address: String, date: String, image: String){
        garbageNameLabel.text = name
        addressLabel.text = address
        dateLabel.text = date
        
        let placeholder = UIImage(named: Constant.placeHoderImage)
        guard let imageUrl = URL(string: image) else { return }
        garbageImage.setImage(url: imageUrl, placeholder: placeholder)
    }
    
}
