//
//  TestTableViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/13/22.
//

import UIKit

class ListGarbageViewController: BaseViewController {
    
    @IBOutlet weak var tableSortView: UIView!
    @IBOutlet weak var collectionSortView: UIView!
    
    @IBOutlet weak var tableSortButton: UIButton!
    @IBOutlet weak var collectionSortButton: UIButton!
    @IBOutlet weak var dateSortButton: UIButton!
    
    private var isTableSortSelected = true
    private var isCollectionSortSelected = false
    private var isDateSortSelected = false
    
    private var isAscending = false
    private var currentPageASC = 0
    private var currentPageDESC = 0
    private var perPage = 10
    
    private var tableVC: GarbageCollectionScheduleTableViewController!
    private var collectionVC: GarbageCollectionScheduleCollectionViewController!
    
    private var viewModel: GarbageCollectionViewModel!
    
    func inject(viewModel: GarbageCollectionViewModel) {
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showNavigationBar()
        updateSortBar()
        setupBinding()
        configView()
        
        getData()
        configLoadMoreCallBack()
    }
    
    private func getData(){
        viewModel.getListASC(currentPage: currentPageASC + 1, perPage: 10)
        viewModel.getListDESC(currentPage: currentPageDESC + 1, perPage: 10)
    }
    
    func refreshData(){ // pull to refresh -> call from HomeViewController
        currentPageASC = 0
        currentPageDESC = 0
        tableVC.emptyList()
        collectionVC.emptyList()
        getData()
    }
    
    private func configLoadMoreCallBack(){
        tableVC.loadMoreCallBack = { [weak self] in
            self?.getData()
            self?.tableVC.stopLoadMoreIndicator()
        }
        collectionVC.loadMoreCallBack = { [weak self] in
            self?.getData()
            self?.collectionVC.stopLoadMoreIndicator()
        }
    }
    
    private func configView(){
        tableVC = GarbageCollectionScheduleTableViewController()
        collectionVC = GarbageCollectionScheduleCollectionViewController()
        
        addChildViewController(controller: tableVC, containerView: tableSortView)
        addChildViewController(controller: collectionVC, containerView: collectionSortView)
        
    }
    
    @IBAction func tableSortButtonPressed(_ sender: UIButton) {
        if !isTableSortSelected {
            isTableSortSelected = true
            isCollectionSortSelected = false
            isDateSortSelected = false
            updateSortBar()
        }
    }
    
    @IBAction func collectionSortButtonPressed(_ sender: UIButton) {
        if !isCollectionSortSelected {
            isTableSortSelected = false
            isCollectionSortSelected = true
            isDateSortSelected = false
            updateSortBar()
        }
    }
    
    @IBAction func dateSortButtonPressed(_ sender: UIButton) {
        if !isDateSortSelected {
            isTableSortSelected = false
            isCollectionSortSelected = false
            isDateSortSelected = true
            updateSortBar()
        }else{
            isAscending = !isAscending
            sortListGarbage()
            updateSortBar()
        }
    }
    
    private func sortListGarbage(){
        if isAscending {
            self.tableVC.isAscending = true
            self.collectionVC.isAscending = true
            self.tableVC.reloadList()
            self.collectionVC.reloadList()
        }else{
            self.tableVC.isAscending = false
            self.collectionVC.isAscending = false
            self.tableVC.reloadList()
            self.collectionVC.reloadList()
        }
    }
    private func updateSortBar(){
        if isTableSortSelected {
            tableSortView.isHidden = false
            collectionSortView.isHidden = true
            
            tableSortButton.setImage(UIImage(named: "ic-list-green.png"), for: .normal)
            collectionSortButton.setImage(UIImage(named: "ic-them-gray.png"), for: .normal)
            if isAscending {
                dateSortButton.setImage(UIImage(named: "ic-sort-up-gray"), for: .normal)
            }else{
                dateSortButton.setImage(UIImage(named: "ic-sort-down-gray"), for: .normal)
            }
            
        }else if isCollectionSortSelected{
            tableSortView.isHidden = true
            collectionSortView.isHidden = false
            
            tableSortButton.setImage(UIImage(named: "ic-list-gray.png"), for: .normal)
            collectionSortButton.setImage(UIImage(named: "ic-them-green.png"), for: .normal)
            if isAscending {
                dateSortButton.setImage(UIImage(named: "ic-sort-up-gray"), for: .normal)
            }else{
                dateSortButton.setImage(UIImage(named: "ic-sort-down-gray"), for: .normal)
            }
        }
        else if isDateSortSelected{
            tableSortButton.setImage(UIImage(named: "ic-list-gray.png"), for: .normal)
            collectionSortButton.setImage(UIImage(named: "ic-them-gray.png"), for: .normal)
            
            if isAscending {
                dateSortButton.setImage(UIImage(named: "ic-sort-up-green"), for: .normal)
            }else{
                dateSortButton.setImage(UIImage(named: "ic-sort-down-green"), for: .normal)
            }
        }
    }
    
    //MARK: - SetupBinding
    private func setupBinding() {
        viewModel.emitEventError
            .subscribeNext { [weak self] error in
                guard self != nil else {return}
                Logger.log("Error: \(error.message)")
            }.disposed(by: rx.disposeBag)
        
        viewModel.emitEventLoading
            .subscribeNext { [weak self] isLoading in
                guard let self = self else {return}
                Logger.log("Loading: \(isLoading)")
//                if isLoading {
//                    self.showLoadingView()
//                } else {
//                    self.hideLoadingView()
//                }
            }.disposed(by: rx.disposeBag)
        
        viewModel.getListASCTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                if data.count != 0 {
                    self.tableVC.updateListASC(listGarbage: data)
                    self.collectionVC.updateListASC(listGarbage: data)
                    self.sortListGarbage()
                    self.currentPageASC += 1
                }
            }.disposed(by: rx.disposeBag)
        
        viewModel.getListDESCTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                if data.count != 0 {
                    self.tableVC.updateListDESC(listGarbage: data)
                    self.collectionVC.updateListDESC(listGarbage: data)
                    self.sortListGarbage()
                    self.currentPageDESC += 1
                }
            }.disposed(by: rx.disposeBag)
    }
    
}
