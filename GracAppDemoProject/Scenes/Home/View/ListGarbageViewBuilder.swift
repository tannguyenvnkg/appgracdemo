//
//  TestTableViewControllerBuilder.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/13/22.
//

import UIKit

class ListGarbageViewBuilder {
    static func build() -> ListGarbageViewController{
        let rootVC = ListGarbageViewController(nibName: "ListGarbageViewController", bundle: nil)
        let interactor = GarbageCollectionInteractorImpl()
        let viewModel = GarbageCollectionViewModel(interactor: interactor)
        rootVC.inject(viewModel: viewModel)
        return rootVC
    }
}
