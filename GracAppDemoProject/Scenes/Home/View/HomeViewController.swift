//
//  WelcomeViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/8/22.
//

import UIKit
import RxSwift
import RxCocoa
import NotificationBannerSwift

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var topBarView: UIView!
    @IBOutlet weak var topbarImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var bannerPageControl: UIPageControl!
    @IBOutlet weak var listGrabageView: UIView!
    
    @IBOutlet weak var garbageCollectionFeeStackView: UIStackView!
    @IBOutlet weak var dumpGarbageWayStackView: UIStackView!
    @IBOutlet weak var sendingComplaintStackView: UIStackView!
    @IBOutlet weak var gameStackView: UIStackView!
    @IBOutlet weak var newStackView: UIStackView!
    
    // developing
    @IBOutlet weak var avatarButton: UIImageView!
    @IBOutlet weak var qrButton: UIImageView!
    @IBOutlet weak var notificationButton: UIImageView!
    var imageArray : [String] = ["bg-banner","bg-banner","bg-banner"]
    
    private var searchBarEmptyErrorBanner: NotificationBanner!
    private var emptyCustomerErrorBanner: NotificationBanner!
    private var searchUserViewModel: SearchUserViewModel!
    
    private var pullToRefreshControl = UIRefreshControl()
    private let listGarbageVC = ListGarbageViewBuilder.build()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.delegate = self
        searchBar.delegate = self
        configTopbar()
        configBanner()
        configView()
        configPullToRefreshControl()
        setupBinding()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = ""
        hideNavigationBar()
        showTabbar()
    }
    
    //MARK: - Config and set up Binding
    override func configNotificationBanner() {
        super.configNotificationBanner()
        searchBarEmptyErrorBanner = NotificationBanner(title: "THÔNG BÁO", subtitle: "Vui lòng điền mã khách hàng bạn muốn tra cứu", style: .danger)
        searchBarEmptyErrorBanner.duration = 1
        
        emptyCustomerErrorBanner = NotificationBanner(title: "THÔNG BÁO", subtitle: "Không tìm thấy khách hàng", style: .danger)
        emptyCustomerErrorBanner.duration = 1
    }
    
    
    func configView(){
        addChildViewController(controller: listGarbageVC, containerView: listGrabageView)
        
        // event for menu bar stackview
        garbageCollectionFeeStackView.formatActionView(selector: #selector(gotoSearchFeeViewController), target: self)
        dumpGarbageWayStackView.formatActionView(selector: #selector(goToDumpGarbageWayViewController), target: self)
        sendingComplaintStackView.formatActionView(selector: #selector(goToComplaintViewController), target: self)
        gameStackView.formatActionView(selector: #selector(goToGameViewController), target: self)
        newStackView.formatActionView(selector: #selector(goToNewsViewController), target: self)
        
        // developing
        avatarButton.formatActionView(selector: #selector(showPopupDeveloping), target: self)
        qrButton.formatActionView(selector: #selector(showPopupDeveloping), target: self)
        notificationButton.formatActionView(selector: #selector(showPopupDeveloping), target: self)
    }
    
    private func configBanner(){
        bannerCollectionView.delegate = self
        bannerCollectionView.dataSource = self
        bannerCollectionView.register(BannerCollectionViewCell.getNib(), forCellWithReuseIdentifier: BannerCollectionViewCell.identifier)
        
        bannerPageControl.currentPage = 0
        bannerPageControl.numberOfPages = imageArray.count
    }
    
    private func configTopbar(){
        topbarImage.image = UIImage(named: "bg-search")
        topbarImage.isHidden = true
        searchBar.searchTextField.backgroundColor = .white
        let searchUserInteractor = SearchUserInteractorImpl()
        self.searchUserViewModel = SearchUserViewModel(interactor: searchUserInteractor)
    }
    
    private func configPullToRefreshControl(){
        pullToRefreshControl.attributedTitle = NSAttributedString(string: "Đang tải dữ liệu mới nhất")
        pullToRefreshControl.addTarget(self, action: #selector(refreshListData(_:)), for: .valueChanged)
        scrollView.refreshControl = pullToRefreshControl
    }
    
    @objc private func refreshListData(_ sender: Any) {
        self.pullToRefreshControl.endRefreshing()
        listGarbageVC.refreshData()
    }
    
    private func setupBinding() {
        // search event
        searchUserViewModel.emitEventError
            .subscribeNext { [weak self] error in
                guard self != nil else {return}
                
                if error.code == 6{
                    self?.errorConnectionBanner.show()
                }else if error.message == "Không tìm thấy khách hàng"{
                    self?.emptyCustomerErrorBanner.show()
                }
                
                Logger.log("Error: \(error.message)")
            }.disposed(by: rx.disposeBag)
        
        searchUserViewModel.emitEventLoading
            .subscribeNext { [weak self] isLoading in
                guard let self = self else {return}
                Logger.log("Loading: \(isLoading)")
                if isLoading {
                    self.showLoadingView()
                } else {
                    self.hideLoadingView()
                }
            }.disposed(by: rx.disposeBag)
        
        searchUserViewModel.getSearchUserByUserCodeActionTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                if data.count != 0 {
                    let userInfoVC = UserInfoViewBuilder.build(userInfo: data[0])
                    self.navigationController?.pushViewController(userInfoVC, animated: true)
                }
            }.disposed(by: rx.disposeBag)
        
        searchUserViewModel.getSearchUserByAddressActionTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                if data.count != 0 {
                    let userInfoVC = UserInfoViewBuilder.build(userInfo: data[0])
                    self.navigationController?.pushViewController(userInfoVC, animated: true)
                }
            }.disposed(by: rx.disposeBag)
    }
    
    @objc func showPopupDeveloping(){
        let popup = PopupDevelopingViewBuilder.build()
        popup.show()
    }
    
    @objc func goToDumpGarbageWayViewController(){
        let dumpGarbageWayVC = DumpGarbageWayViewBuilder.build()
        navigationController?.pushViewController(dumpGarbageWayVC, animated: true)
    }
    
    @objc func gotoSearchFeeViewController(){
        let searchFeeVC = SearchGarbageCollectionFeeViewBuilder.build()
        navigationController?.pushViewController(searchFeeVC, animated: true)
    }
    
    @objc func goToComplaintViewController(){
        let complaintVC = ComplaintViewBuilder.build()
        navigationController?.pushViewController(complaintVC, animated: true)
    }
    
    @objc func goToGameViewController(){
        let gameVC = GameViewBuilder.build()
        navigationController?.pushViewController(gameVC, animated: true)
    }
    @objc func goToNewsViewController(){
        let newsVC = NewsViewBuilder.build()
        navigationController?.pushViewController(newsVC, animated: true)
    }
}

//MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.bannerCollectionView.bounds.size
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = bannerCollectionView.dequeueReusableCell(withReuseIdentifier: BannerCollectionViewCell.identifier, for: indexPath) as! BannerCollectionViewCell
        cell.backgroundImageView.image = UIImage(named: imageArray[indexPath.row])
        
        cell.bookingCallback = { [weak self] in
            self?.tabBarController?.selectedIndex = 2
        }
        cell.searchFeeCallback = { [weak self] in
            self?.gotoSearchFeeViewController()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        bannerPageControl.currentPage = indexPath.row
    }
}

//MARK: - UIScrollViewDelegate
extension HomeViewController : UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
            UIView.animate(withDuration: 0.2) {
                self.topBarView.alpha = 0.5
            }
            
            let scrollOffset = self.scrollView.contentOffset.y
            
            // set image topbar when scroll
            if (scrollOffset > 0){
                self.topbarImage.isHidden = false
            }
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
            UIView.animate(withDuration: 0.2) {
                self.topBarView.alpha = 1
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            
            let scrollOffset = self.scrollView.contentOffset.y
            // hide image if we are at topbar
            if (scrollOffset == 0){
                self.topbarImage.isHidden = true
            }
        }
    }
}

extension HomeViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        if searchText.trim().isEmpty {
            searchBarEmptyErrorBanner.show()
        }else{
            searchUserViewModel.getUserInfoByUserCode(userCode: searchText)
        }
        
    }
    
}
