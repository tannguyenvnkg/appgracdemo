//
//  GarbageCollectionModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import Foundation
struct GarbageCollectionModel: Codable {
    var id: Int = -1
    var name: String = ""
    var address: String = ""
    var latitude: String = ""
    var longitude: String = ""
    var picture1: String = ""
    var picture2: String = ""
    var picture3: String = ""
    var dateSubmit: String = ""
    
    enum CodingKeys: String, CodingKey {
        case id
        case name = "ten_khach_hang"
        case address = "dia_chi"
        case latitude = "map_lat"
        case longitude = "map_lng"
        case picture1 = "picture_1"
        case picture2 = "picture_2"
        case picture3 = "picture_3"
        case dateSubmit = "ngay_dang"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        container.decodeIfPresent(Int.self, forKey: .id, assignTo: &id)
        container.decodeIfPresent(String.self, forKey: .name, assignTo: &name)
        container.decodeIfPresent(String.self, forKey: .address, assignTo: &address)
        container.decodeIfPresent(String.self, forKey: .latitude, assignTo: &latitude)
        container.decodeIfPresent(String.self, forKey: .longitude, assignTo: &longitude)
        container.decodeIfPresent(String.self, forKey: .picture1, assignTo: &picture1)
        container.decodeIfPresent(String.self, forKey: .picture2, assignTo: &picture2)
        container.decodeIfPresent(String.self, forKey: .picture3, assignTo: &picture3)
        container.decodeIfPresent(String.self, forKey: .dateSubmit, assignTo: &dateSubmit)
    }
    
}
