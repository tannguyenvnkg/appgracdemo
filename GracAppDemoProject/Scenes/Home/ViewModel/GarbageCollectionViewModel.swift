//
//  GarbageCollectionViewModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import RxSwift
import RxCocoa
import Action
import NSObject_Rx

class GarbageCollectionViewModel: NSObject {
    private let interactor: GarbageCollectionInteractor
    let emitEventError = PublishRelay<(APIError)>()
    let emitEventLoading = PublishRelay<(Bool)>()
    
    private lazy var getListASCAction = makeGetListASCAction()
    let getListASCTrigger = PublishRelay<[GarbageCollectionModel]>()
    
    private lazy var getListDESCAction = makeGetListDESCAction()
    let getListDESCTrigger = PublishRelay<[GarbageCollectionModel]>()
    
    init(interactor: GarbageCollectionInteractor) {
        self.interactor = interactor
        super.init()
        self.configGetListASCAction()
        self.configGetListDESCAction()
    }

    // MARK: api
    func getListASC(currentPage: Int, perPage: Int) {
        getListASCAction.execute((currentPage,perPage))
    }
    
    func getListDESC(currentPage: Int, perPage: Int) {
        getListDESCAction.execute((currentPage,perPage))
    }
    
    private func makeGetListASCAction() -> Action<(currentPage: Int, perPage: Int), [GarbageCollectionModel]> {
        return Action<(currentPage: Int, perPage: Int), [GarbageCollectionModel]> { [weak self] currentPage, perPage in
            guard let self = self else { return Observable.empty() }
            return self.interactor.getListGarbageCollectionASC(currentPage: currentPage, perPage: perPage).asObservable()
        }
    }
    
    private func makeGetListDESCAction() -> Action<(currentPage: Int, perPage: Int), [GarbageCollectionModel]> {
        return Action<(currentPage: Int, perPage: Int), [GarbageCollectionModel]> { [weak self] currentPage, perPage in
            guard let self = self else { return Observable.empty() }
            return self.interactor.getListGarbageCollectionDESC(currentPage: currentPage, perPage: perPage).asObservable()
        }
    }
    
    private func configGetListASCAction() {
        getListASCAction.elements
            .bind(to: getListASCTrigger)
            .disposed(by: rx.disposeBag)
        
        getListASCAction
            .errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                self?.emitEventLoading.accept(false)
            }
            .disposed(by: rx.disposeBag)
        
        getListASCAction
            .executing
            .subscribeNext { [weak self] executing in
                self?.emitEventLoading.accept(executing)
            }
            .disposed(by: rx.disposeBag)
    }
    
    private func configGetListDESCAction() {
        getListDESCAction.elements
            .bind(to: getListDESCTrigger)
            .disposed(by: rx.disposeBag)
        
        getListDESCAction
            .errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                self?.emitEventLoading.accept(false)
            }
            .disposed(by: rx.disposeBag)
        
        getListDESCAction
            .executing
            .subscribeNext { [weak self] executing in
                self?.emitEventLoading.accept(executing)
            }
            .disposed(by: rx.disposeBag)
    }
}
