//
//  ComplaintInteractor.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 05/07/2022.
//
import Foundation
import RxSwift

protocol ComplaintInteractor {
    func getComplaintCategory() -> Single<([ComplaintModel])>
    func sendComplaint(userComplaint: UserComplaintModel, pictures: [UIImage]) -> Single<ComplaintAcceptModel>

}
