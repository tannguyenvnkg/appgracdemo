//
//  ComplaintInteractorImpl.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 05/07/2022.
//


import Foundation
import RxSwift

struct ComplaintInteractorImpl: ComplaintInteractor {
    let repository = ComplaintRepositoryImpl()
    
    func getComplaintCategory() -> Single<([ComplaintModel])> {
        return repository.getComplaintCategory()
    }
    
    
    func sendComplaint(userComplaint: UserComplaintModel, pictures: [UIImage]) -> Single<ComplaintAcceptModel> {
        return repository.sendComplaint(userComplaint: userComplaint, pictures: pictures)
    }

}
