//
//  ComplaintRepositoryImpl.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 05/07/2022.
//


import Foundation
import RxSwift
import Moya

class ComplaintRepositoryImpl: ComplaintRepository {
   
    let moyaPlugins = [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))]
    
    lazy var apiProvider = MoyaProvider<ComplaintAPI>(plugins: moyaPlugins)
    
    func getComplaintCategory() -> Single<([ComplaintModel])> {
        apiProvider.request(.getComplaintCategory, response: ResponseWrapper<([ComplaintModel])>.self)
    }
    func sendComplaint(userComplaint: UserComplaintModel, pictures: [UIImage]) -> Single<ComplaintAcceptModel> {
        apiProvider.request(.sendComplaint(userComplaint: userComplaint, pictures: pictures), response: ResponseWrapper<(ComplaintAcceptModel)>.self)
    }

}
