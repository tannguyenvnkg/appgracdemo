//
//  DetailComplaintViewController.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 23/06/2022.
//

import UIKit
import RxSwift
import RxCocoa
import NSObject_Rx
import BSImagePicker
import Photos
import NotificationBannerSwift

class DetailComplaintViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userIdTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var choosePictureLabel: UILabel!
    @IBOutlet weak var requiredLabel: UILabel!
    @IBOutlet weak var chooseImageView: UIView!
    @IBOutlet weak var chooseImageButton: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!

    private var complaint: ComplaintModel!
    
    private var imagesSelected: [UIImage] = []
    var selectedImageCallBack: (()->())?
    
    private var textViewPlacehoder = "Nội dung khiếu nại"
    private let placeHolderBorderColor = UIColor(rgb: Constant.placeholderBorderColor)
    private let placeHolderTextColor = UIColor.lightGray
    
    private var chooseImageErrorBanner: NotificationBanner!
    private var successBanner: NotificationBanner!
    private var errorBanner: NotificationBanner!
    
    private var viewModel: ComplaintViewModel!
    
    func inject(viewModel: ComplaintViewModel) {
        self.viewModel = viewModel
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configView()
        setupUI()
        configCollectionView()
        configSelectedImagesCallBack()
        setupBinding()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideTabbar()
        showNavigationBar(showQuestionButton: true,title: complaint?.title.uppercased())
        titleLabel.text = complaint?.title
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    func bindData(complaint: ComplaintModel){
        self.complaint = complaint
    }
    
    private func setupUI(){
        containerView.layer.cornerRadius = 5 
        descriptionTextView.text = "Nội dung khiếu nại"
        descriptionTextView.layer.borderColor = placeHolderBorderColor.cgColor
        descriptionTextView.layer.borderWidth = 2
        descriptionTextView.layer.cornerRadius = 5
        descriptionTextView.textColor = placeHolderTextColor
        descriptionTextView.font = .systemFont(ofSize: 15)

        // set padding for content in textView
        descriptionTextView.contentInset = UIEdgeInsets(top: 0, left: 3.2, bottom: 0, right: 0)
        
        // border color
        userIdTextField.attributedPlaceholder = NSAttributedString(
            string: "Mã khách hàng",
            attributes: [NSAttributedString.Key.foregroundColor: placeHolderTextColor]
        )
        phoneTextField.attributedPlaceholder = NSAttributedString(
            string: "Số điện thoại",
            attributes: [NSAttributedString.Key.foregroundColor: placeHolderTextColor]
        )
        
        submitButton.layer.cornerRadius = 5
        requiredLabel.font = .italicSystemFont(ofSize: 14)
        chooseImageView.layer.borderColor = placeHolderBorderColor.cgColor
        chooseImageView.layer.borderWidth = 2
        chooseImageView.layer.cornerRadius = 5
        updateCollectionView()
    }
    
    //MARK: - config and setup binding
    override func configNotificationBanner(){
        super.configNotificationBanner()
        
        chooseImageErrorBanner = NotificationBanner(title: "THÔNG BÁO", subtitle: "Vui lòng chọn ảnh", style: .danger)
        chooseImageErrorBanner.duration = 2
        
        successBanner = NotificationBanner(title: "THÀNH CÔNG", subtitle: "Gửi khiếu nại thành công", style: .success)
        successBanner.duration = 2
        
        errorBanner = NotificationBanner(title: "THẤT BẠI", subtitle: "Gửi khiếu nại thất bại", style: .danger)
        errorBanner.duration = 2
        
    }
    
    private func configView(){
        descriptionTextView.delegate = self
        chooseImageButton.formatActionView(selector: #selector(chooseImage), target: self)
    }
    
    private func configCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(ImageSelectedCollectionViewCell.getNib(), forCellWithReuseIdentifier: ImageSelectedCollectionViewCell.identifier)
    }
    
    private func setupBinding() {
        viewModel.emitEventError
            .subscribeNext { [weak self] error in
                guard self != nil else {return}
                if error.code == 6{
                    self?.errorConnectionBanner.show()
                }
                Logger.log("Error: \(error.message)")
            }.disposed(by: rx.disposeBag)

        viewModel.emitEventLoading
            .subscribeNext { [weak self] isLoading in
                guard let self = self else {return}
                Logger.log("Loading: \(isLoading)")
                if isLoading {
                    self.showLoadingView()
                } else {
                    self.hideLoadingView()
                }
            }.disposed(by: rx.disposeBag)

        viewModel.sendComplaintActionTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                if data.accept {
                    self.successBanner.show()
                    // clear text
                    self.phoneTextField.text = ""
                    self.userIdTextField.text = ""
                    self.descriptionTextView.text = self.textViewPlacehoder
                    self.descriptionTextView.textColor = self.placeHolderTextColor
                }else{
                    self.errorBanner.show()
                }
            }.disposed(by: rx.disposeBag)


    }
    
    //MARK: - choose image
    private func configSelectedImagesCallBack(){
        self.selectedImageCallBack = { [weak self] in
            self?.updateCollectionView()
        }
    }
    
    private func updateCollectionView(){
        if imagesSelected.count == 0 {
            self.collectionViewHeight.constant = 0
        }else{
            self.collectionViewHeight.constant = 130
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.collectionView.reloadData()
        }
    }
    
    @objc func chooseImage(){
        
        let imagePicker = ImagePickerController()
        imagePicker.settings.selection.max = 3
        imagePicker.settings.theme.selectionStyle = .numbered
        imagePicker.settings.fetch.assets.supportedMediaTypes = [.image]
        imagePicker.settings.selection.unselectOnReachingMax = true
        
        let options = PHImageRequestOptions()
        options.version = .original
        options.isSynchronous = true
        options.isNetworkAccessAllowed = true
    
        self.presentImagePicker(imagePicker, select: { (asset) in
        }, deselect: { (asset) in
        }, cancel: { (assets) in
        }, finish: { (assets) in
            self.imagesSelected.removeAll()
            var i = 0
            for asset in assets{
                PHImageManager.default().requestImage(for: asset, targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight), contentMode: .aspectFill, options: options) { (image, info) in
                    if i < assets.count{
                        print("\(i < assets.count)")
                        i = i + 1
                        if let image = image {
                            guard let imageResize = image.resizedTo1MB() else {return}
                            self.imagesSelected.append(imageResize)
                        }
                    }
                    if i >= assets.count {
                        self.selectedImageCallBack?()
                    }
                    
                }
            }
        })
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        if descriptionTextView.text.trim().isEmpty || (userIdTextField.text ?? "").trim().isEmpty || (phoneTextField.text ?? "").trim().isEmpty || descriptionTextView.text.trim() == textViewPlacehoder {
            
            fillFieldErrorBanner.show()
        }else if imagesSelected.count < 1{
            chooseImageErrorBanner.show()
        }else{

            let customerName = "Nguyễn Phước Tấn"
            let customerPhone = phoneTextField.text!.trim()
            let customerEmail = "npttpn@gmail.com"
            let description = descriptionTextView.text.trim()
            let complaintCategoryID = complaint.id

            let userComplaint = UserComplaintModel(customerName: customerName, customerPhone: customerPhone, customerEmail: customerEmail, description: description, complaintCategoryID: "\(complaintCategoryID)")
            

            viewModel.sendComplaint(userComplaint: userComplaint, pictures: imagesSelected)
        }
    }


}

extension DetailComplaintViewController: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == placeHolderTextColor {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = textViewPlacehoder
            textView.textColor = placeHolderTextColor
        }
    }
}
//MARK: -  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension DetailComplaintViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesSelected.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageSelectedCollectionViewCell.identifier, for: indexPath) as! ImageSelectedCollectionViewCell
        
        cell.setData(image: imagesSelected[indexPath.row])
        cell.removeImageCallBack = { [weak self] in
            self?.imagesSelected.remove(at: indexPath.row)
            self?.updateCollectionView()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/3 - 10
        let height = collectionView.bounds.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("item \(indexPath) selected")
    }
    
}
