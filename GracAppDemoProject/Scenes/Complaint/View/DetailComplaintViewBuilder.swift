//
//  DetailComplaintViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 23/06/2022.
//

struct DetailComplaintViewBuilder {
    static func build(complaint: ComplaintModel) -> DetailComplaintViewController {
        let rootVC = DetailComplaintViewController(nibName: "DetailComplaintViewController", bundle: nil)
        let interactor = ComplaintInteractorImpl()
        let viewModel = ComplaintViewModel(interactor: interactor)
        rootVC.inject(viewModel: viewModel)
        rootVC.bindData(complaint: complaint)
        return rootVC
    }
}
