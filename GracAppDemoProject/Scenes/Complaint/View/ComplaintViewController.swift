//
//  ComplaintViewController.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 23/06/2022.
//

import UIKit
import RxSwift
import RxCocoa
import NSObject_Rx

class ComplaintViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    
    private var arrayComplaint: [ComplaintModel] = []
//    private var arrayComplaint = ComplaintModel.getData()
    private var viewModel: ComplaintViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        setupBinding()
        configTableView()
        getData()
    }
    private func getData(){
        viewModel.getComplaintCategory()
    }
    func inject(viewModel: ComplaintViewModel) {
        self.viewModel = viewModel
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideTabbar()
        showNavigationBar(showQuestionButton: true,title: "CHỌN LOẠI HÌNH KHIẾU NẠI")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    private func setupUI(){
        containerView.layer.cornerRadius = 10
        nextButton.layer.cornerRadius = 5
    }
    
    private func configTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ComplaintTableViewCell.getNib(), forCellReuseIdentifier: ComplaintTableViewCell.identifier)
    }
    
    private func setupBinding() {
        viewModel.emitEventError
            .subscribeNext { [weak self] error in
                guard self != nil else {return}
                if error.code == 6{
                    self?.errorConnectionBanner.show()
                }
                
                Logger.log("Error: \(error.message)")
            }.disposed(by: rx.disposeBag)
        
        viewModel.emitEventLoading
            .subscribeNext { [weak self] isLoading in
                guard let self = self else {return}
                Logger.log("Loading: \(isLoading)")
                if isLoading {
                    self.showLoadingView()
                } else {
                    self.hideLoadingView()
                }
            }.disposed(by: rx.disposeBag)
        
        viewModel.getComplaintCategoryActionTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                self.arrayComplaint.append(contentsOf: data)
                self.tableView.reloadData()
            }.disposed(by: rx.disposeBag)
        
        
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        guard let indexPath = tableView.indexPathForSelectedRow else { return }
        let complaint = arrayComplaint[indexPath.row]
        let detailComplaintVC = DetailComplaintViewBuilder.build(complaint: complaint)
        navigationController?.pushViewController(detailComplaintVC, animated: true)
    }
    
}
extension ComplaintViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayComplaint.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ComplaintTableViewCell.identifier, for: indexPath) as! ComplaintTableViewCell
    
        cell.setData(title: arrayComplaint[indexPath.row].title)
        
        cell.selectionStyle = .none
        return cell
    }
    
}
