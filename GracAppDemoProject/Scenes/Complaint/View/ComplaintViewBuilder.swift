//
//  ComplaintViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 23/06/2022.
//

struct ComplaintViewBuilder {
    static func build() -> ComplaintViewController {
        let rootVC = ComplaintViewController(nibName: "ComplaintViewController", bundle: nil)
        let interactor = ComplaintInteractorImpl()
        let viewModel = ComplaintViewModel(interactor: interactor)
        rootVC.inject(viewModel: viewModel)
        return rootVC
    }
}
