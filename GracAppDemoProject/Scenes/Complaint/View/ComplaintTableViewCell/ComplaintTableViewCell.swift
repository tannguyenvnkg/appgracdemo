//
//  ComplaintTableViewCell.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 23/06/2022.
//

import UIKit

class ComplaintTableViewCell: UITableViewCell {

    @IBOutlet weak var checkBoxImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    static let identifier = "ComplaintTableViewCell"
    
    static func getNib() -> UINib{
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            checkBoxImage.image = UIImage(named: "ic-checkbox-checked")
        }else{
            checkBoxImage.image = UIImage(named: "ic-checkbox")
        }
    }
    
    func setData(title: String){
        titleLabel.text = title
    }
    
}
