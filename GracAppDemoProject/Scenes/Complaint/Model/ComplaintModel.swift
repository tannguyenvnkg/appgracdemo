//
//  ComplaintModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 23/06/2022.
//
import Foundation

struct ComplaintModel : Codable{
    
    var id: Int = -1
    var title: String = ""
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
    }
    
    init(id: Int = -1, title: String = "") {
        self.id = id
        self.title = title
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        container.decodeIfPresent(Int.self, forKey: .id, assignTo: &id)
        container.decodeIfPresent(String.self, forKey: .title, assignTo: &title)
        
    }
    
  static func getData() -> [ComplaintModel]{
        let arr = [
            ComplaintModel(id: 1, title: "Chất lượng dịch vụ"),
            ComplaintModel(id: 2, title: "Giá tiền rác"),
            ComplaintModel(id: 3, title: "Loại rác thu gom"),
            ComplaintModel(id: 4, title: "Thời gian thu gom"),
            ComplaintModel(id: 5, title: "Khác")
        ]
        return arr
    }
}
