//
//  UserComplaintModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 06/07/2022.
//

import Foundation

struct UserComplaintModel{
    var customerName: String
    var customerPhone: String
    var customerEmail: String
    var description: String
    var complaintCategoryID: String
    
    init(customerName: String, customerPhone: String, customerEmail: String, description: String, complaintCategoryID: String) {
        self.customerName = customerName
        self.customerPhone = customerPhone
        self.customerEmail = customerEmail
        self.description = description
        self.complaintCategoryID = complaintCategoryID
    }
}
