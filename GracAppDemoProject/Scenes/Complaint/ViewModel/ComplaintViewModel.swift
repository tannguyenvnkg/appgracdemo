//
//  ComplaintViewModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 05/07/2022.
//

import Foundation
import RxSwift
import RxCocoa
import Action
import NSObject_Rx

class ComplaintViewModel: NSObject {
    
    private let interactor: ComplaintInteractor
    let emitEventError = PublishRelay<(APIError)>()
    let emitEventLoading = PublishRelay<(Bool)>()
    
    private lazy var getComplaintCategoryAction = makeGetComplaintCategoryAction()
    let getComplaintCategoryActionTrigger = PublishRelay<[ComplaintModel]>()
  
    private lazy var sendComplaintAction = makesendComplaintAction()
    let sendComplaintActionTrigger = PublishRelay<ComplaintAcceptModel>()

    
    init(interactor: ComplaintInteractorImpl) {
        self.interactor = interactor
        super.init()
        self.configGetComplaintCategoryAction()
        self.configSendComplaintAction()

    }
    
    //MARK: Api to get data
    func getComplaintCategory() {
        getComplaintCategoryAction.execute()
    }

    func sendComplaint(userComplaint: UserComplaintModel, pictures: [UIImage]){
        sendComplaintAction.execute((userComplaint,pictures))
    }

    private func makeGetComplaintCategoryAction() -> Action<(), [ComplaintModel]> {
        return Action<(), [ComplaintModel]> {[weak self] _ in
            guard let self = self else { return Observable.empty() }
            return self.interactor.getComplaintCategory().asObservable()
        }
    }
    
    private func makesendComplaintAction() -> Action<(UserComplaintModel,[UIImage]), ComplaintAcceptModel> {
        return Action<(UserComplaintModel,[UIImage]), ComplaintAcceptModel> { [weak self] data in
            guard let self = self else { return Observable.empty() }
            return self.interactor.sendComplaint(userComplaint: data.0, pictures: data.1).asObservable()
        }
    }

 
    private func configGetComplaintCategoryAction() {
        getComplaintCategoryAction.elements
            .bind(to: getComplaintCategoryActionTrigger)
            .disposed(by: rx.disposeBag)
        
        getComplaintCategoryAction.errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                guard let self = self else {return}
                self.emitEventError.accept(error)
                self.emitEventLoading.accept(false)
            }.disposed(by: rx.disposeBag)
        
        getComplaintCategoryAction.executing
            .subscribeNext { [weak self] executing in
                guard let self = self else {return}
                self.emitEventLoading.accept(executing)
            }.disposed(by: rx.disposeBag)
    }
    
    private func configSendComplaintAction() {
        sendComplaintAction.elements
            .bind(to: sendComplaintActionTrigger)
            .disposed(by: rx.disposeBag)
        
        sendComplaintAction.errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                guard let self = self else {return}
                self.emitEventError.accept(error)
                self.emitEventLoading.accept(false)
            }.disposed(by: rx.disposeBag)
        
        sendComplaintAction.executing
            .subscribeNext { [weak self] executing in
                guard let self = self else {return}
                self.emitEventLoading.accept(executing)
            }.disposed(by: rx.disposeBag)
    }
}
