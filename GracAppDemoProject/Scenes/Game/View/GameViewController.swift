//
//  GameViewController.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/16/22.
//

import UIKit

class GameViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private let arrayTitle = ["Game phân loại rác cho bé", "Game phân loại rác",
                              "Game giải cứu rác nhựa", "Trắc nghiệm về rác thải"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.layer.cornerRadius = 10
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(GameTableViewCell.getNib(), forCellReuseIdentifier: GameTableViewCell.identifier)
        tableView.rowHeight = 65
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showTabbar()
        showNavigationBar(showQuestionButton: true,title: "GAME")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }


}

extension GameViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GameTableViewCell.identifier, for: indexPath) as! GameTableViewCell
    
        cell.setData(title: arrayTitle[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var urlString = ""
        
        var isLandscape = true
        switch indexPath.row {
        case 0:
            urlString = "https://grac.vn/game2/"
        case 1:
            urlString = "https://grac.vn/game/"
        case 2:
            urlString = "https://grac.vn/game3"
        case 3:
            urlString = "https://grac.vn/20-cau-hoi-trac-nghiem-ve-quan-ly-va-phan-loai-chat-thai/"
            isLandscape = false
        default:
            urlString = "https://grac.vn/"
            isLandscape = false
        }
        
        let url = URL(string: urlString)
        let gracWebVC = GracWebViewBuilder.build(url: url!,isLandscape: isLandscape)
        navigationController?.pushViewController(gracWebVC, animated: true)
    }
    
}
