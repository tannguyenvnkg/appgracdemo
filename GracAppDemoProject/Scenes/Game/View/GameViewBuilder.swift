//
//  GameViewBuilder.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/16/22.
//

struct GameViewBuilder {
    static func build() -> GameViewController {
        let rootVC = GameViewController(nibName: "GameViewController", bundle: nil)
        return rootVC
    }
}
