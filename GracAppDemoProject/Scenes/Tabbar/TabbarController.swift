//
//  Tabbar.swift
//  GracAppDemoProject
//
//  Created by Vu Mai Hoang Hai Hung on 6/14/22.
//

import UIKit

class TabbarController : UITabBarController{
    var homeTabNavigationController : UINavigationController!
    var bookingTabNavigationController : UINavigationController!
    var supportTabNavigationController : UINavigationController!
    var questionTabNavigationController : UINavigationController!
    var walletTabNavigationController : UINavigationController!
    
    override func viewDidLoad() {
        homeTabNavigationController = UINavigationController.init(rootViewController: HomeViewBuilder.build())
        supportTabNavigationController = UINavigationController.init(rootViewController: SupportViewBuilder.build())
        bookingTabNavigationController =  UINavigationController.init(rootViewController: BookingViewBuilder.build())
        questionTabNavigationController =  UINavigationController.init(rootViewController: QuestionViewBuilder.build())
        walletTabNavigationController = UINavigationController.init(rootViewController: WalletViewBuilder.build())
        viewControllers = [homeTabNavigationController,supportTabNavigationController, bookingTabNavigationController ,questionTabNavigationController,walletTabNavigationController]
        
        let size = CGSize(width: 24, height: 24)
        let homeImage = resizeImage(image: UIImage(named: "ic-trangchu-gray")!,targetSize: size)
        let supportImage = resizeImage(image: UIImage(named: "ic-hotro-gray")!,targetSize: size)
        let bookingImage = resizeImage(image: UIImage(named: "ic-datlich-gray")!,targetSize: size)
        let questionImage = resizeImage(image: UIImage(named: "ic-cauhoi-gray")!,targetSize: size)
        let walletImage = resizeImage(image: UIImage(named: "ic-vi-gray")!,targetSize: size)
        
        
        let item1 = UITabBarItem(title: "Trang chủ", image: homeImage, tag: 0)
        let item2 = UITabBarItem(title: "Hỗ trợ", image: supportImage, tag: 1)
        let item3 = UITabBarItem(title: "Đặt lịch", image:  bookingImage, tag: 2)
        let item4 = UITabBarItem(title: "Câu hỏi", image:  questionImage, tag: 3)
        let item5 = UITabBarItem(title: "Ví", image:  walletImage, tag: 4)
        
        homeTabNavigationController.tabBarItem = item1
        supportTabNavigationController.tabBarItem = item2
        bookingTabNavigationController.tabBarItem = item3
        questionTabNavigationController.tabBarItem = item4
        walletTabNavigationController.tabBarItem = item5
        
        
        UITabBar.appearance().tintColor = UIColor(rgb: 0x1B9340)
        UITabBar.appearance().barTintColor = UIColor.white
        
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            overrideUserInterfaceStyle = .light
        }
    }
    
    private func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage? {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(origin: .zero, size: newSize)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
