//
//  CityRepository.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import Foundation
import RxSwift

protocol AddressRepository{
    func getProvince() -> Single<[AddressModel]>
    func getDistrict(id: Int) -> Single<[AddressModel]>
    func getWard(id: Int) -> Single<[AddressModel]>
}

