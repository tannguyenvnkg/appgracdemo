//
//  CityRepositoryImpl.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import Foundation
import RxSwift
import Moya

class AddressRepositoryImpl: AddressRepository{

    let moyaPlugins = [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))]
   
    lazy var apiProvider = MoyaProvider<GarbageAPI>(plugins: moyaPlugins)

    func getProvince() -> Single<[AddressModel]> {
        apiProvider.request(.getProvince, response: ResponseWrapper<[AddressModel]>.self)
    }
    
    func getDistrict(id: Int) -> Single<[AddressModel]> {
        apiProvider.request(.getDistrict(id), response: ResponseWrapper<[AddressModel]>.self)
    }
    
    func getWard(id: Int) -> Single<[AddressModel]> {
        apiProvider.request(.getWard(id), response: ResponseWrapper<[AddressModel]>.self)
    }
    
}
