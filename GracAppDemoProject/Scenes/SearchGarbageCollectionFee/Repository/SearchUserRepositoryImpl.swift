//
//  UserInfoRepositoryImpl.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 04/07/2022.
//
import Foundation
import RxSwift
import Moya

class SearchUserRepositoryImpl: SearchUserRepository {
    let moyaPlugins = [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))]
    lazy var apiProvider = MoyaProvider<CustomerAPI>(plugins: moyaPlugins)
    
    
    func searchUserByUserCode(userCode: String) -> Single<[SearchUserModel]> {
        apiProvider.request(.searchUserByUserCode(userCode), response: ResponseWrapper<[SearchUserModel]>.self)
    }
    
    func searchUserByAddress(address: String) -> Single<[SearchUserModel]> {
        apiProvider.request(.searchUserByAddress(address), response: ResponseWrapper<[SearchUserModel]>.self)
    }
}
