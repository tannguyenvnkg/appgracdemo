//
//  SearchGarbageCollectionFeeViewController.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 27/06/2022.
//

import UIKit
import DropDown
import RxSwift
import RxCocoa
import NotificationBannerSwift

enum OptionSearch{
    case userId
    case address
}

class SearchGarbageCollectionFeeViewController: BaseViewController{
    
    @IBOutlet weak var provinceDropDownView: UIView!
    @IBOutlet weak var provinceDropDownLabel: UILabel!
    @IBOutlet weak var districtDropDownView: UIView!
    @IBOutlet weak var districtDropDownLabel: UILabel!
    @IBOutlet weak var wardDropDownView: UIView!
    @IBOutlet weak var wardDropDownLabel: UILabel!
//
    @IBOutlet weak var userIdRadiobutton: UIStackView!
    @IBOutlet weak var addressRadioButton: UIStackView!
    @IBOutlet weak var userIdRadiobuttonImage: UIImageView!
    @IBOutlet weak var addressRadioButtonImage: UIImageView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var provinceTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var provinceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var districtTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var districtHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var wardTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var wardHeightConstraint: NSLayoutConstraint!

    private var option: OptionSearch = .userId
    private let placeHolderBorderColor = UIColor(rgb: 0xB1B1B2)
    private let placeHolderTextColor = UIColor(rgb: 0x6B6B6B)
    
    private let provinceDropDown = DropDown()
    private var arrayProvince: [AddressModel] = []
    private var arrayProvinceName: [String] = []
    private let districtDropDown = DropDown()
    private var arrayDistrict: [AddressModel] = []
    private var arrayDistrictName: [String] = []
    private let wardDropDown = DropDown()
    private var arrayWard: [AddressModel] = []
    private var arrayWardName: [String] = []
    
    private var chooseProviceErrorBanner: NotificationBanner!
    private var chooseDistrictErrorBanner: NotificationBanner!
    private var chooseWardErrorBanner: NotificationBanner!
    private var emptyCustomerErrorBanner: NotificationBanner!
    
    private var addressViewModel: AddressViewModel!
    private var searchUserViewModel: SearchUserViewModel!
    
    func inject(addressViewModel: AddressViewModel, searchUserViewModel: SearchUserViewModel) {
        self.addressViewModel = addressViewModel
        self.searchUserViewModel = searchUserViewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        arrayProvince.append(contentsOf: ProvinceModel.getData())
        //        arrayProvinceName.append(contentsOf: ProvinceModel.getTitle(array: arrayProvince))
        
        setupUI()
        configView()
        configDropDown()
        setupBinding()
        
        addressViewModel.getProvince()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideTabbar()
        showNavigationBar(showQuestionButton: true,title: "TRA CỨU TIỀN RÁC")
        updateRadioButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    override func configNotificationBanner() {
        super.configNotificationBanner()
        chooseProviceErrorBanner = NotificationBanner(title: "THÔNG BÁO", subtitle: "Vui lòng chọn tỉnh / thành phố", style: .danger)
        chooseProviceErrorBanner.duration = 2
        chooseDistrictErrorBanner = NotificationBanner(title: "THÔNG BÁO", subtitle: "Vui lòng chọn quận / huyện", style: .danger)
        chooseDistrictErrorBanner.duration = 2
        chooseWardErrorBanner = NotificationBanner(title: "THÔNG BÁO", subtitle: "Vui lòng chọn phường / xã", style: .danger)
        chooseWardErrorBanner.duration = 2
        
        emptyCustomerErrorBanner = NotificationBanner(title: "THÔNG BÁO", subtitle: "Không tìm thấy khách hàng", style: .danger)
        emptyCustomerErrorBanner.duration = 2
    }
    
    private func setupUI(){
        containerView.layer.cornerRadius = 5
        submitButton.layer.cornerRadius = 5
        
        searchTextField.borderStyle = .none
        searchTextField.textColor = UIColor.darkText
        searchTextField.font = .systemFont(ofSize: 17)
        searchTextField.attributedPlaceholder = NSAttributedString(
            string: "Mã khách hàng",
            attributes: [NSAttributedString.Key.foregroundColor: placeHolderTextColor]
        )
        
        searchView.layer.borderColor = placeHolderBorderColor.cgColor
        searchView.layer.borderWidth = 1
        searchView.layer.cornerRadius = 20
        
        provinceDropDownLabel.addUnderline(spacing: 10)
        districtDropDownLabel.addUnderline(spacing: 10)
        wardDropDownLabel.addUnderline(spacing: 10)
        
        hideAddressDropDown(isHide: (option == .userId))
    }
    
    private func configView(){
        userIdRadiobutton.formatActionView(selector: #selector(chooseUserIdOption), target: self)
        addressRadioButton.formatActionView(selector: #selector(chooseAddressOption), target: self)
        
    }
    
    private func configDropDown(){
        provinceDropDownView.backgroundColor = UIColor.white
        provinceDropDown.cellHeight = 50
        provinceDropDown.anchorView = provinceDropDownView
        provinceDropDown.bottomOffset = CGPoint(x: 0, y:(provinceDropDown.anchorView?.plainView.bounds.height)!)
        provinceDropDown.direction = .bottom
        
        provinceDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            provinceDropDownLabel.text = arrayProvince[index].fullName
            addressViewModel.getDistrict(id: arrayProvince[index].id)
            if let districtIndexPath = districtDropDown.indexPathForSelectedRow {
                districtDropDown.deselectRow(districtIndexPath.row)
                districtDropDownLabel.text = "Chọn Quận / Huyện"
            }
            if let wardIndexPath = wardDropDown.indexPathForSelectedRow {
                wardDropDown.deselectRow(wardIndexPath.row)
                wardDropDownLabel.text = "Chọn Phường / Xã"
            }
            
            
        }

        districtDropDownView.backgroundColor = UIColor.white
        districtDropDown.cellHeight = 50
        districtDropDown.anchorView = districtDropDownView
        districtDropDown.bottomOffset = CGPoint(x: 0, y:(districtDropDown.anchorView?.plainView.bounds.height)!)
        districtDropDown.direction = .bottom

        districtDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            districtDropDownLabel.text = arrayDistrict[index].fullName
            addressViewModel.getWard(id: arrayDistrict[index].id)
            if let wardIndexPath = wardDropDown.indexPathForSelectedRow {
                wardDropDown.deselectRow(wardIndexPath.row)
                wardDropDownLabel.text = "Chọn Phường / Xã"
            }
        }

        wardDropDownView.backgroundColor = UIColor.white
        wardDropDown.cellHeight = 50
        wardDropDown.anchorView = wardDropDownView
        wardDropDown.bottomOffset = CGPoint(x: 0, y:(wardDropDown.anchorView?.plainView.bounds.height)!)
        wardDropDown.direction = .bottom

        wardDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            wardDropDownLabel.text = arrayWard[index].fullName
        }
    }
    
    @objc func chooseUserIdOption(){
        self.option = .userId
        updateRadioButton()
        hideAddressDropDown(isHide: (option == .userId))

    }
    
    @objc func chooseAddressOption(){
        self.option = .address
        updateRadioButton()
        hideAddressDropDown(isHide: (option == .userId))

    }
    
    private func updateRadioButton(){
        switch self.option {
        case .userId:
            userIdRadiobuttonImage.image = UIImage(named: "ic-radio-button-checked")
            addressRadioButtonImage.image = UIImage(named: "ic-radio-button")
            searchTextField.placeholder = "Mã khách hàng"
        case .address:
            userIdRadiobuttonImage.image = UIImage(named: "ic-radio-button")
            addressRadioButtonImage.image = UIImage(named: "ic-radio-button-checked")
            searchTextField.placeholder = "Địa chỉ"
        }
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        if (searchTextField.text ?? "").trim().isEmpty{
            fillFieldErrorBanner.show()
        }
        else{
            switch option {
            case .userId:
                let userCode = searchTextField.text!
                searchUserViewModel.getUserInfoByUserCode(userCode: userCode)
                
            case .address:
                guard let provincePosition = provinceDropDown.indexForSelectedRow else { chooseProviceErrorBanner.show(); return }
                guard let districtPosition = districtDropDown.indexForSelectedRow else { chooseDistrictErrorBanner.show(); return }
                guard let wardPosition = wardDropDown.indexForSelectedRow else { chooseWardErrorBanner.show(); return }
                let province = arrayProvince[provincePosition].fullName
                let district = arrayDistrict[districtPosition].fullName
                let ward = arrayWard[wardPosition].fullName
                let content = searchTextField.text!.trim()
                let address = "\(content), \(ward), \(district), \(province)"
                searchUserViewModel.getUserInfoByAddress(address: address)
            }
            
        }
    }
    
    private func hideAddressDropDown(isHide: Bool){
        if isHide {
            provinceHeightConstraint.constant = 0
            provinceTopConstraint.constant = 0
            provinceDropDownView.isHidden = true
            
            districtHeightConstraint.constant = 0
            districtTopConstraint.constant = 0
            districtDropDownView.isHidden = true
            
            wardHeightConstraint.constant = 0
            wardTopConstraint.constant = 0
            wardDropDownView.isHidden = true
        }else{
            provinceHeightConstraint.constant = 40
            provinceTopConstraint.constant = 20
            provinceDropDownView.isHidden = false
            
            districtHeightConstraint.constant = 40
            districtTopConstraint.constant = 10
            districtDropDownView.isHidden = false
            
            wardHeightConstraint.constant = 40
            wardTopConstraint.constant = 10
            wardDropDownView.isHidden = false
        }
    }

    @IBAction func provinceDrownDownPressed(_ sender: UIButton) {
        provinceDropDown.show()
    }
    
    @IBAction func districtDrownDownPressed(_ sender: UIButton) {
        districtDropDown.show()
    }
    
    @IBAction func wardDrownDownPressed(_ sender: UIButton) {
        wardDropDown.show()
    }
    
    private func setupBinding() {
        // address event
        addressViewModel.emitEventError
            .subscribeNext { [weak self] error in
                guard self != nil else {return}
                if error.code == 6{
                    self?.errorConnectionBanner.show()
                }

                Logger.log("Error: \(error.message)")
            }.disposed(by: rx.disposeBag)
        
        addressViewModel.emitEventLoading
            .subscribeNext { [weak self] isLoading in
                guard let self = self else {return}
                Logger.log("Loading: \(isLoading)")
                if isLoading {
                    self.showLoadingView()
                } else {
                    self.hideLoadingView()
                }
            }.disposed(by: rx.disposeBag)
        
        addressViewModel.getProvinceActionTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                self.arrayProvince.append(contentsOf: data)
                self.arrayProvinceName.append(contentsOf: AddressModel.getTitle(array: self.arrayProvince))
                self.provinceDropDown.dataSource = self.arrayProvinceName
            }.disposed(by: rx.disposeBag)
        
        addressViewModel.getDistirctActionTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                self.arrayDistrict.removeAll()
                self.arrayDistrictName.removeAll()
                self.arrayDistrict.append(contentsOf: data)
                self.arrayDistrictName.append(contentsOf: AddressModel.getTitle(array: self.arrayDistrict))
                self.districtDropDown.dataSource = self.arrayDistrictName
                
                self.arrayWard.removeAll()
                self.arrayWardName.removeAll()
                self.wardDropDown.dataSource = self.arrayWardName
            }.disposed(by: rx.disposeBag)

        addressViewModel.getWardActionTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                self.arrayWard.removeAll()
                self.arrayWardName.removeAll()
                self.arrayWard.append(contentsOf: data)
                self.arrayWardName.append(contentsOf: AddressModel.getTitle(array: self.arrayWard))
                self.wardDropDown.dataSource = self.arrayWardName
            }.disposed(by: rx.disposeBag)
//
        // search event
        searchUserViewModel.emitEventError
            .subscribeNext { [weak self] error in
                guard self != nil else {return}
                if error.code == 6{
                    self?.errorConnectionBanner.show()
                }else if error.message == "Không tìm thấy khách hàng"{
                    self?.emptyCustomerErrorBanner.show()
                }

                Logger.log("Error: \(error.message)")
            }.disposed(by: rx.disposeBag)
        
        searchUserViewModel.emitEventLoading
            .subscribeNext { [weak self] isLoading in
                guard let self = self else {return}
                Logger.log("Loading: \(isLoading)")
                if isLoading {
                    self.showLoadingView()
                } else {
                    self.hideLoadingView()
                }
            }.disposed(by: rx.disposeBag)
        
        searchUserViewModel.getSearchUserByUserCodeActionTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                if data.count != 0 {
                    let userInfoVC = UserInfoViewBuilder.build(userInfo: data[0])
                    self.navigationController?.pushViewController(userInfoVC, animated: true)
                }
            }.disposed(by: rx.disposeBag)
        
        searchUserViewModel.getSearchUserByAddressActionTrigger
            .subscribeNext { [weak self] data in
                guard let self = self else {return}
                if data.count != 0 {
                    let userInfoVC = UserInfoViewBuilder.build(userInfo: data[0])
                    self.navigationController?.pushViewController(userInfoVC, animated: true)
                }
            }.disposed(by: rx.disposeBag)
    }
}

