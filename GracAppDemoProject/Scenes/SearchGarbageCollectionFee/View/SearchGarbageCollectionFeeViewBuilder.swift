//
//  SearchGarbageCollectionFeeViewBuilder.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 27/06/2022.
//

struct SearchGarbageCollectionFeeViewBuilder {
    static func build() -> SearchGarbageCollectionFeeViewController {
        let rootVC = SearchGarbageCollectionFeeViewController(nibName: "SearchGarbageCollectionFeeViewController", bundle: nil)
        let addressInteractor = AddressInteractorImpl()
        let searchUserInteractor = SearchUserInteractorImpl()
        
        let addressViewModel = AddressViewModel(interactor: addressInteractor)
        let searchUserViewModel = SearchUserViewModel(interactor: searchUserInteractor)
        rootVC.inject(addressViewModel: addressViewModel, searchUserViewModel: searchUserViewModel)
        return rootVC
    }
}

