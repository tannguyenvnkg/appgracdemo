//
//  UserInfoModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 04/07/2022.
//

import Foundation

struct SearchUserModel: Codable {
    
    var userCode: String = ""
    var userName: String = ""
    var address: String = ""
    var serviceCode: String = ""
    var servicePrice: String = ""
    var tax: Float = -1
    var transportFee: String = ""
    var collectionFee: String = ""
    var destructionFee: String = ""
    var debt: String = ""
    var billingCircle: String = ""
    var contractID: Int = -1
    var contractCode: String = ""
    var totalMoney: String = ""
    var isPaid: Int = -1
    var paymentMethod: String? = ""
    var detailPaymentID: String = ""
    var staffName: String = ""
    var staffPhone: String = ""
    var isRegistration: Int = -1
    var specialAddress: String? = ""
    var userID: Int = -1
    var momoFee: String = ""
    
    
    enum CodingKeys: String, CodingKey {
        case userCode = "ma_khach_hang"
        case userName = "ten_khach_hang"
        case address = "dia_chi"
        case serviceCode = "ma_dich_vu"
        case servicePrice = "gia_dich_vu"
        case tax = "thue"
        case transportFee = "tien_van_chuyen"
        case collectionFee = "tien_thu_gom"
        case destructionFee = "tien_xu_ly"
        case debt = "cong_no"
        case billingCircle = "ky_thanh_toan"
        case contractID = "hop_dong_id"
        case contractCode = "ma_hop_dong"
        case totalMoney = "tong_so_tien_can_thanh_toan"
        case isPaid = "trang_thai_thanh_toan"
        case paymentMethod = "phuong_thuc_thanh_toan"
        case detailPaymentID = "id_thanh_toan_chi_tiet"
        case staffName = "ten_nhan_vien_thu_tien"
        case staffPhone = "sdt_nhan_vien_thu_tien"
        case isRegistration = "is_phan_loai_rac_nguon"
        case specialAddress = "text_dia_chi_dac_biet"
        case userID = "khach_hang_id"
        case momoFee = "fee_momo"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        container.decodeIfPresent(String.self, forKey: .userCode, assignTo: &userCode)
        container.decodeIfPresent(String.self, forKey: .userName, assignTo: &userName)
        container.decodeIfPresent(String.self, forKey: .address, assignTo: &address)
        container.decodeIfPresent(String.self, forKey: .serviceCode, assignTo: &serviceCode)
        container.decodeIfPresent(String.self, forKey: .servicePrice, assignTo: &servicePrice)
        container.decodeIfPresent(Float.self, forKey: .tax, assignTo: &tax)
        container.decodeIfPresent(String.self, forKey: .transportFee, assignTo: &transportFee)
        container.decodeIfPresent(String.self, forKey: .collectionFee, assignTo: &collectionFee)
        container.decodeIfPresent(String.self, forKey: .destructionFee, assignTo: &destructionFee)
        container.decodeIfPresent(String.self, forKey: .debt, assignTo: &debt)
        container.decodeIfPresent(String.self, forKey: .billingCircle, assignTo: &billingCircle)
        container.decodeIfPresent(Int.self, forKey: .contractID, assignTo: &contractID)
        container.decodeIfPresent(String.self, forKey: .contractCode, assignTo: &contractCode)
        container.decodeIfPresent(String.self, forKey: .totalMoney, assignTo: &totalMoney)
        container.decodeIfPresent(Int.self, forKey: .isPaid, assignTo: &isPaid)
        container.decodeIfPresent(String?.self, forKey: .paymentMethod, assignTo: &paymentMethod)
        container.decodeIfPresent(String.self, forKey: .detailPaymentID, assignTo: &detailPaymentID)
        container.decodeIfPresent(String.self, forKey: .staffName, assignTo: &staffName)
        container.decodeIfPresent(String.self, forKey: .staffPhone, assignTo: &staffPhone)
        container.decodeIfPresent(Int.self, forKey: .isRegistration, assignTo: &isRegistration)
        container.decodeIfPresent(String?.self, forKey: .specialAddress, assignTo: &specialAddress)
        container.decodeIfPresent(Int.self, forKey: .userID, assignTo: &userID)
        container.decodeIfPresent(String.self, forKey: .momoFee, assignTo: &momoFee)
    }
    
    init(userCode: String = "", userName: String = "", address: String = "", serviceCode: String = "", servicePrice: String = "", tax: Float = -1, transportFee: String = "", collectionFee: String = "", destructionFee: String = "", debt: String = "", billingCircle: String = "", contractID: Int = -1, contractCode: String = "", totalMoney: String = "", isPaid: Int = -1, paymentMethod: String? = "", detailPaymentID: String = "", staffName: String = "", staffPhone: String = "", isRegistration: Int = -1, specialAddress: String? = "", userID: Int = -1,momoFee: String = "") {
        self.userCode = userCode
        self.userName = userName
        self.address = address
        self.serviceCode = serviceCode
        self.servicePrice = servicePrice
        self.tax = tax
        self.transportFee = transportFee
        self.collectionFee = collectionFee
        self.destructionFee = destructionFee
        self.debt = debt
        self.billingCircle = billingCircle
        self.contractID = contractID
        self.contractCode = contractCode
        self.totalMoney = totalMoney
        self.isPaid = isPaid
        self.paymentMethod = paymentMethod
        self.detailPaymentID = detailPaymentID
        self.staffName = staffName
        self.staffPhone = staffPhone
        self.isRegistration = isRegistration
        self.specialAddress = specialAddress
        self.userID = userID
        self.momoFee = momoFee
    }

}
