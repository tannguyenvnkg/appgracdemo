//
//  CityModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 28/06/2022.
//
import Foundation

struct ProvinceModel: Codable{
    
    var id: Int = -1
    var name: String = ""
    var fullName: String = ""

    init(id: Int, name: String, fullName: String){
        self.id = id
        self.name = name
        self.fullName = fullName
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "ten"
        case fullName = "full_name"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        container.decodeIfPresent(Int.self, forKey: .id, assignTo: &id)
        container.decodeIfPresent(String.self, forKey: .name, assignTo: &name)
        container.decodeIfPresent(String.self, forKey: .fullName, assignTo: &fullName)
    }
    
    static func getData() -> [ProvinceModel]{
        let arrayData = [
            ProvinceModel(id: 1, name: "Hà Nội", fullName: "Thành Phố Hà Nội"),
            ProvinceModel(id: 2, name: "Hồ Chí Minh", fullName: "Thành Phố Hồ Chí Minh"),
            ProvinceModel(id: 3, name: "Kiên Giang", fullName: "Tỉnh Kiên Giang"),
        ]
        return arrayData
    }
    
    static func getTitle(array: [ProvinceModel]) -> [String]{
        var arrayTitle: [String] = []
        for item in array {
            arrayTitle.append(item.fullName)
        }
        return arrayTitle
    }
}
