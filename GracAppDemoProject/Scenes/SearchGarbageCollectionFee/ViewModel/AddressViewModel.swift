//
//  CityViewModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import Foundation
import RxSwift
import RxCocoa
import Action
import NSObject_Rx

class AddressViewModel: NSObject {
    
    private let interactor: AddressInteractor
    let emitEventError = PublishRelay<(APIError)>()
    let emitEventLoading = PublishRelay<(Bool)>()

    private lazy var getProvinceAction = makeGetProvinceAction()
    let getProvinceActionTrigger = PublishRelay<[AddressModel]>()

    private lazy var getDistrictAction = makeGetDistrictAction()
    let getDistirctActionTrigger = PublishRelay<[AddressModel]>()
    
    private lazy var getWardAction = makeGetWardAction()
    let getWardActionTrigger = PublishRelay<[AddressModel]>()
    
    init(interactor: AddressInteractorImpl) {
        self.interactor = interactor
        super.init()
        self.configGetProvinceAction()
        self.configGetDistrictAction()
        self.configGetWardAction()
    }

    //MARK: Api to get data
    func getProvince() {
        getProvinceAction.execute()
    }
    
    func getDistrict(id: Int) {
        getDistrictAction.execute(id)
    }
    
    func getWard(id: Int) {
        getWardAction.execute(id)
    }
    
    private func makeGetProvinceAction() -> Action<(), [AddressModel]> {
        return Action<(), [AddressModel]> { [weak self] in
            guard let self = self else { return Observable.empty() }
            return self.interactor.getProvince().asObservable()
        }
    }
    
    private func makeGetDistrictAction() -> Action<Int, [AddressModel]> {
        return Action<Int, [AddressModel]> { [weak self] id in
            guard let self = self else { return Observable.empty() }
            return self.interactor.getDistrict(id: id).asObservable()
        }
    }
    
    private func makeGetWardAction() -> Action<Int, [AddressModel]> {
        return Action<Int, [AddressModel]> { [weak self] id in
            guard let self = self else { return Observable.empty() }
            return self.interactor.getWard(id: id).asObservable()
        }
    }
   
    
    private func configGetProvinceAction() {
        getProvinceAction.elements
            .bind(to: getProvinceActionTrigger)
            .disposed(by: rx.disposeBag)
        
        getProvinceAction.errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                guard let self = self else {return}
                self.emitEventError.accept(error)
                self.emitEventLoading.accept(false)
            }.disposed(by: rx.disposeBag)
        
        getProvinceAction.executing
            .subscribeNext { [weak self] executing in
                guard let self = self else {return}
                self.emitEventLoading.accept(executing)
            }.disposed(by: rx.disposeBag)
    }
    
    private func configGetDistrictAction() {
        getDistrictAction.elements
            .bind(to: getDistirctActionTrigger)
            .disposed(by: rx.disposeBag)
        
        getDistrictAction.errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                guard let self = self else {return}
                self.emitEventError.accept(error)
                self.emitEventLoading.accept(false)
            }.disposed(by: rx.disposeBag)
        
        getDistrictAction.executing
            .subscribeNext { [weak self] executing in
                guard let self = self else {return}
                self.emitEventLoading.accept(executing)
            }.disposed(by: rx.disposeBag)
    }
    
    private func configGetWardAction() {
        getWardAction.elements
            .bind(to: getWardActionTrigger)
            .disposed(by: rx.disposeBag)
        
        getWardAction.errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                guard let self = self else {return}
                self.emitEventError.accept(error)
                self.emitEventLoading.accept(false)
            }.disposed(by: rx.disposeBag)
        
        getWardAction.executing
            .subscribeNext { [weak self] executing in
                guard let self = self else {return}
                self.emitEventLoading.accept(executing)
            }.disposed(by: rx.disposeBag)
    }
}
