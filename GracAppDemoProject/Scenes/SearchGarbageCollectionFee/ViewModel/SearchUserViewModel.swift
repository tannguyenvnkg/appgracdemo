//
//  UserInfoViewModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 04/07/2022.
//


import Foundation
import RxSwift
import RxCocoa
import Action
import NSObject_Rx

class SearchUserViewModel: NSObject {
    
    private let interactor: SearchUserInteractor
    let emitEventError = PublishRelay<(APIError)>()
    let emitEventLoading = PublishRelay<(Bool)>()
    
    private lazy var getSearchUserByUserCodeAction = makeGetSearchUserByUserCodeAction()
    let getSearchUserByUserCodeActionTrigger = PublishRelay<[SearchUserModel]>()
    
    private lazy var getSearchUserByAddressAction = makeGetSearchUserByAddressAction()
    let getSearchUserByAddressActionTrigger = PublishRelay<[SearchUserModel]>()
    
    init(interactor: SearchUserInteractorImpl) {
        self.interactor = interactor
        super.init()
        self.configSearchUserByUserCodeAction()
        self.configSearchUserByAddressAction()
    }
    
    //MARK: Api to get data
    func getUserInfoByUserCode(userCode: String) {
        getSearchUserByUserCodeAction.execute(userCode)
    }
    
    func getUserInfoByAddress(address: String) {
        getSearchUserByAddressAction.execute(address)
    }
    
    private func makeGetSearchUserByUserCodeAction() -> Action<String, [SearchUserModel]> {
        return Action<String, [SearchUserModel]> {[weak self] userCode in
            guard let self = self else { return Observable.empty() }
            return self.interactor.searchUserByUserCode(userCode: userCode).asObservable()
        }
    }
    
    private func makeGetSearchUserByAddressAction() -> Action<String, [SearchUserModel]> {
        return Action<String, [SearchUserModel]> {[weak self] address in
            guard let self = self else { return Observable.empty() }
            return self.interactor.searchUserByAddress(address: address).asObservable()
        }
    }
    
    private func configSearchUserByUserCodeAction() {
        getSearchUserByUserCodeAction.elements
            .bind(to: getSearchUserByUserCodeActionTrigger)
            .disposed(by: rx.disposeBag)
        
        getSearchUserByUserCodeAction.errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                guard let self = self else {return}
                self.emitEventError.accept(error)
                self.emitEventLoading.accept(false)
            }.disposed(by: rx.disposeBag)
        
        getSearchUserByUserCodeAction.executing
            .subscribeNext { [weak self] executing in
                guard let self = self else {return}
                self.emitEventLoading.accept(executing)
            }.disposed(by: rx.disposeBag)
    }
    
    private func configSearchUserByAddressAction() {
        getSearchUserByAddressAction.elements
            .bind(to: getSearchUserByAddressActionTrigger)
            .disposed(by: rx.disposeBag)
        
        getSearchUserByAddressAction.errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                guard let self = self else {return}
                self.emitEventError.accept(error)
                self.emitEventLoading.accept(false)
            }.disposed(by: rx.disposeBag)
        
        getSearchUserByAddressAction.executing
            .subscribeNext { [weak self] executing in
                guard let self = self else {return}
                self.emitEventLoading.accept(executing)
            }.disposed(by: rx.disposeBag)
    }
   
}
