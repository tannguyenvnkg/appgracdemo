//
//  CityViewModel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import Foundation
import RxSwift
import RxCocoa
import Action
import NSObject_Rx

class ProvinceViewModel: NSObject {
    
    private let interactor: ProvinceInteractor
    let emitEventError = PublishRelay<(APIError)>()
    let emitEventLoading = PublishRelay<(Bool)>()

    private lazy var getDataProvinceAction = makeGetDataAction()
    let getDataProvinceActionTrigger = PublishRelay<[ProvinceModel]>()

    init(interactor: ProvinceInteractorImpl) {
        self.interactor = interactor
        super.init()
        self.configGetDataProvinceAction()
    }

    //MARK: Api to get data
    func getDataProvince() {
        getDataProvinceAction.execute()
    }
    
    private func makeGetDataAction() -> Action<(), [ProvinceModel]> {
        return Action<(), [ProvinceModel]> { [weak self] in
            guard let self = self else { return Observable.empty() }
            return self.interactor.getProvince().asObservable()
        }
    }
    
    private func configGetDataProvinceAction() {
        getDataProvinceAction.elements
            .bind(to: getDataProvinceActionTrigger)
            .disposed(by: rx.disposeBag)
        
        getDataProvinceAction.errors
            .apiErrorMessage
            .subscribeNext { [weak self] error in
                guard let self = self else {return}
                self.emitEventError.accept(error)
                self.emitEventLoading.accept(false)
            }.disposed(by: rx.disposeBag)
        
        getDataProvinceAction.executing
            .subscribeNext { [weak self] executing in
                guard let self = self else {return}
                self.emitEventLoading.accept(executing)
            }.disposed(by: rx.disposeBag)
    }
}
