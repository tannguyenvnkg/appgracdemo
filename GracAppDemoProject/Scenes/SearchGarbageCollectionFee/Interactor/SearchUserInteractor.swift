//
//  UserInfoInteractor.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 04/07/2022.
//


import Foundation
import RxSwift

protocol SearchUserInteractor {
    func searchUserByUserCode(userCode: String) -> Single<[SearchUserModel]>
    func searchUserByAddress(address: String) -> Single<[SearchUserModel]>
}
