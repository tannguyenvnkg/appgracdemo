//
//  CityInteractorImpl.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import Foundation
import RxSwift

struct AddressInteractorImpl: AddressInteractor {

    let repository = AddressRepositoryImpl()
    
    func getProvince() -> Single<[AddressModel]> {
        return repository.getProvince()
    }
    
    func getDistrict(id: Int) -> Single<[AddressModel]> {
        return repository.getDistrict(id: id)
    }
    
    func getWard(id: Int) -> Single<[AddressModel]> {
        return repository.getWard(id: id)
    }
}
