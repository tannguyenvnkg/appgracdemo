//
//  CityInteractorImpl.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import Foundation
import RxSwift

struct ProvinceInteractorImpl: ProvinceInteractor {
    let repository = ProvinceRepositoryImpl()
    
    func getProvince() -> Single<[ProvinceModel]> {
        return repository.getProvince()
    }
    
}
