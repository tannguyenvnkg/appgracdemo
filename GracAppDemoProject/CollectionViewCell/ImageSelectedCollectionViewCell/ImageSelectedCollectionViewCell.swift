//
//  ImageSelectedCollectionViewCell.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 01/07/2022.
//

import UIKit

class ImageSelectedCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var deleteButton: UIView!
    
    var removeImageCallBack: (()->())?
    static let identifier = "ImageSelectedCollectionViewCell"
    
    static func getNib() -> UINib{
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        deleteButton.formatActionView(selector: #selector(removeImage), target: self)
        deleteButton.layer.cornerRadius = deleteButton.frame.size.width/2
    }

    @objc private func removeImage(){
        self.removeImageCallBack?()
    }
    
    func setData(image: UIImage){
        self.image.image = image
    }
    
}
