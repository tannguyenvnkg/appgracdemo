import Foundation
import RxSwift
import RxCocoa
import RxSwiftExt
import Moya
import Action

extension ObservableType {
    
    public func forwardError<O: ObserverType>(to observer: O) -> Observable<Element> where O.Element == Error {
        return self.do(onError: { observer.onNext($0) })
    }
}

extension PrimitiveSequence where Trait == SingleTrait, Element == Response {
    
    func catchAPIError(_ type: APIErrorResponse.Type) -> Single<Element> {
        return flatMap { response in
            var message = ""
            if (200...399).contains(response.statusCode) {
                
                do {
                    if let json = try? JSONSerialization.jsonObject(with: response.data, options: []) as? [String: AnyObject] {
                        if let isError = json["error"] as? Bool, isError == true {
                            message = json["message"] as? String ?? ""
                            
                        } else {
                            return .just(response)
                        }
                    }
                }
            //    return .just(response)
            } else if (400...599).contains(response.statusCode) {
                do {
                    if response.statusCode == 401 { // token invalid
                        message = "Token invalid"
                        throw APIError(code: 401, message: "Token invalid")
                    }
                    var apiErrorResponse = try response.map(type.self) as APIErrorResponse
                    if let apiError = apiErrorResponse.apiError(code: response.statusCode) {
                        throw apiError
                    }
                    throw APIError.init()
                } catch {
                    throw error
                }
            }
            throw APIError(code: 0, message: message)
        }
    }
    
}

extension SharedSequenceConvertibleType where SharingStrategy == DriverSharingStrategy, Element == Error {
    
    var apiErrorMessage: Driver<APIError> {
        return flatMap { error in
            if let actionError = error as? ActionError {
                switch actionError {
                case .underlyingError(let apiError as APIError):
                    return .just(apiError)
                default:
                    return .empty()
                }
            }
            return .just(APIError())
        }
    }
}

extension ObservableType where Element == ActionError {
    
    var apiErrorMessage: Observable<APIError> {
        return flatMap { actionError -> Observable<APIError> in
            switch actionError {
            case .underlyingError(let apiError as APIError):
                return .just(apiError)
            case .underlyingError(let error as NSError):
                Logger.log("error: \(error.code)")
                return .just(APIError(code: 6, message: "Vui lòng kiểm tra lại kêt nối internet"))
            default:
                return .empty()
            }
        }
    }
}
