//
//  String.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 02/07/2022.
//
import UIKit
extension String {
    func trim(using characterSet: CharacterSet = .whitespacesAndNewlines) -> String {
        return trimmingCharacters(in: characterSet)
    }
}
