//
//  UILabel.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 28/06/2022.
//

import UIKit

extension UILabel {
//    func addUnderline(){
//        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
//        let underlineAttributedString = NSAttributedString(string: self.text ?? "", attributes: underlineAttribute)
//        self.attributedText = underlineAttributedString
//    }
    
    func addUnderline(spacing: Int = 1){
        let line = UIView()
        line.translatesAutoresizingMaskIntoConstraints = false
        line.backgroundColor = UIColor(rgb: 0xE6E7E7)
        self.addSubview(line)
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", metrics: nil, views: ["line":line]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[line(1)]-(\(-spacing))-|", metrics: nil, views: ["line":line]))
    }
}
