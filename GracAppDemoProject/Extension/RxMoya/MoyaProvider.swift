//
//  MoyaProvider.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 29/06/2022.
//

import RxSwift
import Moya

extension MoyaProvider {
    func request<R: Decodable>(_ api: Target, response: ResponseWrapper<R>.Type) -> Single<R> {
        return rx
            .request(api)
            .catchAPIError(APIErrorResponse.self)
            .map(response.self)
            .map { $0.data }
    }
    
    func requestEmptyResponse(_ api: Target) -> Single<()> {
        return rx
            .request(api)
            .catchAPIError(APIErrorResponse.self)
            .asObservable()
            .mapTo(())
            .asSingle()
    }
}
