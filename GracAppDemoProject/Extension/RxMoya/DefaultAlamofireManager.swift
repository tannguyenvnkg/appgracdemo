//
//  DefaultAlamofireManager.swift
//  Hack Kanji
//
//  Created by Vu Mai Hoang Hai Hung on 8/17/21.
//  Copyright © 2021 Vu Mai Hoang Hai Hung. All rights reserved.
//

import Foundation
import Alamofire

class DefaultAlamofireManager: Alamofire.Session {
    static let sharedManager: DefaultAlamofireManager = {
        let configuration = URLSessionConfiguration.default
        configuration.headers = .default
        configuration.timeoutIntervalForRequest = 30 // as seconds, you can set your request timeout
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return DefaultAlamofireManager(configuration: configuration)
    }()
}
