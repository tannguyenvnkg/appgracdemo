import UIKit
extension UIViewController{
    func addChildViewController(controller: UIViewController, containerView: UIView, byConstraints: Bool = false) {
        containerView.addSubview(controller.view)
        addChild(controller)
        controller.didMove(toParent: self)

        if byConstraints {
            guard let subView = controller.view else { return }
          //  subView.snp.makeConstraints { $0.edges.equalToSuperview() }
        } else {
            controller.view.frame = containerView.bounds
        }
    }
}
