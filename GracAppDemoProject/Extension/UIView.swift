//
//  UIView.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 21/06/2022.
//

import UIKit

extension UIView{
    func formatActionView(selector: Selector, target: Any?) {
        self.gestureRecognizers?.forEach(self.removeGestureRecognizer)
        self.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: target, action: selector)
        addGestureRecognizer(tapGestureRecognizer)
    }
}
