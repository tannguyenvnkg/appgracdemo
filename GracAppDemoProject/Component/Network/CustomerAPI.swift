//
//  CustomerAPI.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 04/07/2022.
//


import Foundation
import Moya
import Alamofire

enum CustomerAPI {
    case searchUserByUserCode(String)
    case searchUserByAddress(String)
}

extension CustomerAPI: TargetType {
    
    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
    
    var baseURL: URL {
        switch NetworkManager.environment {
        case .production:
            return NetworkManager.baseUrlProduction
        case .dev:
            return NetworkManager.baseUrlDev
        }
    }
    
    var path: String {
        switch self {
        case .searchUserByUserCode:
            return "/grac-mobile-app/traCuuTienRacByLoginID"
        case .searchUserByAddress:
            return "/grac-mobile-app/traCuuTienRacByAddress"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .searchUserByUserCode, .searchUserByAddress:
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case .searchUserByUserCode(let userCode):
            return .requestParameters(parameters: [
                "token": NetworkManager.token,
                "maKhachHang": userCode
            ], encoding: JSONEncoding.default)
        case .searchUserByAddress(let address):
            return .requestParameters(parameters: [
                "token": NetworkManager.token,
                "customerAddress": address
            ], encoding: JSONEncoding.default)
        }
    }
    
    var sampleData: Data {
        return "[{\"name\": \"Repo Name\"}]".data(using: String.Encoding.utf8)!
    }
}
