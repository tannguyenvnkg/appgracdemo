//
//  NetworkManager.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 04/07/2022.
//

import Foundation

class NetworkManager {
    
    enum Environment {
        case production, dev
    }
    
    static let environment = Environment.dev
    static let token : String = {
        if NetworkManager.environment == .dev {
            return "ab4535e7cf80c3b8067ab00954510610d6874536"
        }
        else{
            return "ab4535e7cf80c3b8067ab00954510610d6874536"
        }
    }()
    
    static let baseUrlDev = URL(string: "https://e.grac.vn/api")!
    static let baseUrlProduction = URL(string: "https://e.grac.vn/api")!
}
