//
//  ComplaintAPI.swift
//  GracAppDemoProject
//
//  Created by BSP21 on 05/07/2022.
//
import Foundation
import Moya
import Alamofire

enum ComplaintAPI {
    case getComplaintCategory
    case sendComplaint(userComplaint: UserComplaintModel, pictures: [UIImage])

}


extension ComplaintAPI: TargetType {
    
    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
    
    var baseURL: URL {
        switch NetworkManager.environment {
        case .production:
            return NetworkManager.baseUrlProduction
        case .dev:
            return NetworkManager.baseUrlDev
        }
    }
    
    var path: String {
        switch self {
        case .getComplaintCategory:
            return "/grac-mobile-app/getCategoryPhanAnhKhieuNai"
        case .sendComplaint:
            return "/grac-mobile-app/addNewKhieuNaiPhanAnh"

        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getComplaintCategory:
            return .post
        case .sendComplaint:
            return .post

        }
    }
    
    var task: Task {
        switch self {
        case .getComplaintCategory:
            return .requestParameters(parameters: ["token": NetworkManager.token], encoding: JSONEncoding.default)
        case .sendComplaint(userComplaint: let userComplaint, pictures: let pictures):
            var multipartData = [
                MultipartFormData(provider: .data(userComplaint.customerName.data(using: .utf8)!), name: "customerUserNane"),
                MultipartFormData(provider: .data(userComplaint.customerPhone.data(using: .utf8)!), name: "customerPhone"),
                MultipartFormData(provider: .data(userComplaint.customerEmail.data(using: .utf8)!), name: "customerEmail"),
                MultipartFormData(provider: .data(userComplaint.description.data(using: .utf8)!), name: "description"),
                MultipartFormData(provider: .data(userComplaint.complaintCategoryID.data(using: .utf8)!), name: "categoryPhanAnhKhieuNai"),
                MultipartFormData(provider: .data(NetworkManager.token.data(using: .utf8)!), name: "token"),
            ]
            
            var i = 0
            for picture in pictures {
                let imageData = picture.jpegData(compressionQuality: 1.0)!
                let formData = MultipartFormData(provider: .data(imageData),
                                                 name: "picture_\(i+1)",
                                                 fileName: "\(Date().timeIntervalSince1970)-picture.jpg",
                                                 mimeType: "image/jpg")
                
                multipartData.append(formData)
                i = i + 1
            }
            return .uploadMultipart(multipartData)

        }
    }
    
    var sampleData: Data {
        return "[{\"name\": \"Repo Name\"}]".data(using: String.Encoding.utf8)!
    }
}
